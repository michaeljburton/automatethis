using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Web;
using System.Diagnostics;
using OpenQA.Selenium.Support.UI;
using AutomateThis.Model;
using AutomateThis.Protractor;
using OpenQA.Selenium.Interactions;

namespace AutomateThis
{
    public  class WebDriverCmds
    {
     
        public IWebDriver WebDriver;
        public NgWebDriver NgWebDriver;
        public Repository Repository;

        public WebDriverCmds( IWebDriver webDriver,NgWebDriver ngWebDriver,Repository repository)
        {
            this.WebDriver = webDriver;
            this.NgWebDriver = ngWebDriver;
            this.Repository = repository;
        }

        //Find Elements
        public  IWebElement FindElementByXPath(string element)
        {             
            WebDriverWait Wait = new WebDriverWait(WebDriver, TimeSpan.FromSeconds(Repository.Util.Config.SeleniumWaitTime));
            IWebElement WebElement = Wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(element)));
            return WebElement;
        }
        public  IWebElement FindElementByCss(string element)
        {  
            WebDriverWait Wait = new WebDriverWait(WebDriver, TimeSpan.FromSeconds(Repository.Util.Config.SeleniumWaitTime));
            IWebElement WebElement = Wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(element)));
            return WebElement;
        }
        public  IWebElement FindElementByClass(string element)
        {  
            WebDriverWait Wait = new WebDriverWait(WebDriver, TimeSpan.FromSeconds(Repository.Util.Config.SeleniumWaitTime));
            IWebElement WebElement = Wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName(element)));
            return WebElement;
        }
        public  IWebElement FindElementByName(string element)
        {  
            WebDriverWait Wait = new WebDriverWait(WebDriver, TimeSpan.FromSeconds(Repository.Util.Config.SeleniumWaitTime));
            IWebElement WebElement = Wait.Until(ExpectedConditions.ElementIsVisible(By.Name(element)));
            return WebElement;
        }
        public  IWebElement FindElementById(string element)
        {  
            WebDriverWait Wait = new WebDriverWait(WebDriver, TimeSpan.FromSeconds(Repository.Util.Config.SeleniumWaitTime));
            IWebElement WebElement = Wait.Until(ExpectedConditions.ElementIsVisible(By.Id(element)));
            return WebElement;
        }
        public  IWebElement FindElementByLinkText(string element)
        {  
            WebDriverWait Wait = new WebDriverWait(WebDriver, TimeSpan.FromSeconds(Repository.Util.Config.SeleniumWaitTime));
            IWebElement WebElement = Wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText(element)));
            return WebElement;
        }
        public  NgWebElement FindElementByModel(string element)
        {  
            NgWebElement WebElement = NgWebDriver.FindElement(NgBy.Model(element));
            return WebElement;
        }
        public  NgWebElement FindElementByBinding(string element)
        {  
            NgWebElement WebElement = NgWebDriver.FindElement(NgBy.Binding(element));
            return WebElement;
        }
        public  NgWebElement FindElementByRepeater(string element)
        {  
            NgWebElement WebElement = NgWebDriver.FindElement(NgBy.Repeater(element));
            return WebElement;
        }


        //Behaviours e.g. Clicking etc... 
        public  void Get(string url)
        {
            WebDriver.Navigate().GoToUrl(url);
        }
         /// <summary>
        /// Wrapper function for selenium function WebDriver.Navigate().Back();
        /// </summary>
        public  void Back() 
        { 
            WebDriver.Navigate().Back();      
        }
        /// <summary>
        /// Wrapper function for selenium function WebDriver.Navigate().Forward();
        /// </summary>
        public  void Forward() { WebDriver.Navigate().Forward(); }
        /// <summary>
        /// Wrapper function for selenium function  WebDriver.Navigate().Refresh();
        /// </summary>
        public  void Refresh() { WebDriver.Navigate().Refresh(); }
        /// <summary>
        /// Wrapper function for selenium function  WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());
        /// </summary>
        public  void SelectNewWindow() { WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last()); }
        /// <summary>
        /// Finds element by subcommands e.g. xpath, css and id etc...
        /// </summary>
         /// <summary>
        /// Wrapper of selenium function SendKeys(element);
        /// </summary>
        public  void InputValue(IWebElement webElement,NgWebElement ngWebElement,string input)
        {
            //input value
            if (webElement != null)
            {
                webElement.Clear();
                webElement.SendKeys(input);
                return;
            }
            if (ngWebElement != null)
            {
                //CurrentNgElement.Clear();
                ngWebElement.SendKeys(input);
                return;
            }
        

        }
        /// <summary>
        ///  Wrapper of selenium function SelectByText(text);
        /// Selects a Drop Down Value by currentElement
        /// </summary>
        public  void SelectDropDownValue(IWebElement webElement,NgWebElement ngWebElement,string value)
        {

            //input value
            if (webElement != null)
            {
                var selectElement = new SelectElement(webElement);
                selectElement.SelectByText(value);
            }
            else
            {
                var selectElement = new SelectElement(ngWebElement);
                selectElement.SelectByText(value);
            }

        }
        /// <summary>
        /// Wrapper of selenium function Clear();
        /// clears the currentElement 
        /// </summary>
        public  void ClearElement(IWebElement webElement)
        {
            // Clear current element
            webElement.Clear();
        }
        /// <summary>
        /// Wrapper of selenium function Click();
        /// clicks the currentElement 
        /// </summary>
        public void ClickElement( IWebElement webElement,NgWebElement ngWebElement)
        {
            // Click Element      
            if (webElement != null)
            {
                Actions actions = new Actions(WebDriver);
                actions.MoveToElement(webElement).Perform();
                webElement.Click();
                return;
            }
            if (ngWebElement != null)
            {
                //CurrentNgElement.Clear();
                ngWebElement.Click();
                return;
            }
           
     
           
        }
        /// <summary>
        /// Wrapper of selenium function GetAttribute("value");
        /// gets attribute value 
        /// </summary>
        public  void GetAttribute(IWebElement webElement,NgWebElement ngWebElement,string compareValue)
        {
            string actualValue;
            if (webElement != null)
            {
                actualValue = webElement.GetAttribute("value");
            }
            else
            {
                actualValue = ngWebElement.GetAttribute("value");
            }
            //assign attribute          
            if(actualValue != compareValue)
            {
               throw new Exception(string.Format("Expected Result: {0} , Actual value found: {1}",compareValue,actualValue));
            }
        }


    }

}
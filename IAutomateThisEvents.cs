using AutomateThis.Model;

namespace AutomateThis
{
    public interface IAutomateThisEvents
    {
        void EventErrorLog(string message,string currentErrorLogLocation);
        void EventLog(string message,string currentLogLocation);
        void EventLog(string message);
        void EventTestRunResults(object obj,string currentLogLocation);

    }
}
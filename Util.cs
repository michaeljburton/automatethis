
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System.Threading;
using System.Security.Cryptography;
using System.Net;
using AutomateThis.Model;


namespace AutomateThis
{
    public class Util
    {
        IAutomateThisEvents AutomateThisEvents;
        public Config Config;
        public string PageConfigLocation = AppDomain.CurrentDomain.BaseDirectory + "PageConfigs";
        public static string ConfigLocation = AppDomain.CurrentDomain.BaseDirectory + "Config.json";
        public string EnvironmentVarsLocation = AppDomain.CurrentDomain.BaseDirectory + "Environment.json";
        string CustomString = "";


        Dictionary<string, Action<int>> Commands;
        public Util(IAutomateThisEvents automateThisEvents,Config config)
        {
            Commands = new Dictionary<string, Action<int>>();
            AddCommands();
            this.AutomateThisEvents = automateThisEvents;
            this.Config = config;
        }
        public string MessageFormatterException(string method, Exception ex)
        {
            var result = string.Format(
                "DateTime: {0} ThreadId: {1} Method: {2} Exception Type: {3} Message: {4}",
                GetTimeStamp(),
                GetThreadId(),
                method,
                ex.GetType().ToString(),
                ex.Message);

            return result;
        }
        public string MessageFormatter(string message)
        {
            var result = string.Format(
                "DateTime: {0} ThreadId: {1} Message: {2}",
                GetTimeStamp(),
                GetThreadId(),
                message);

            return result;
        }
        public string MessageFormatter(object obj, Repository.MessageType messageType)
        {
            string message = "";
            string ResultJson = "";
            try
            {
                ResultJson = SerializeObject(obj);

                if (obj.GetType() == typeof(StepResult))
                {
                    var stepResult = (StepResult)obj;
                    if (stepResult.Exception != null)
                    {
                        message = string.Format(
                       "DateTime: {0} ThreadId: {1} Exception Type: {2} Exception Message: {3} Message Type: {4} Message: \n{5}",
                       GetTimeStamp(),
                       GetThreadId(),
                       stepResult.Exception.GetType().ToString(),
                       stepResult.Exception.Message,
                       messageType.ToString(),
                       ResultJson);

                        return message;
                    }
                }
                message = string.Format(
                     "DateTime: {0} ThreadId: {1}  Message Type: {2} Message: \n{3}",
                     GetTimeStamp(),
                     GetThreadId(),
                     messageType.ToString(),
                     ResultJson);


            }
            catch (Exception ex)
            {
                AutomateThisEvents.EventErrorLog(MessageFormatterException("Util.MessageFormatterStepResult()", ex), Config.ErrorLogLocation);
            }


            return message;
        }
        public string FormatTimeSpan(TimeSpan ts)
        {
            return String.Format("{0}",
            ts.TotalMilliseconds);
        }

        public string GetTimeStamp()
        {
            return DateTime.UtcNow.ToString("yyyyMMddHHmmss");
        }
        public string GetThreadId()
        {
            return Thread.CurrentThread.ManagedThreadId.ToString();
        }
        public string Post(string url, string data, string contentType)
        {
            if (Config.SkipSslCheck == 1)
            {
                ServicePointManager.ServerCertificateValidationCallback = null;
                ServicePointManager.ServerCertificateValidationCallback += (send, certificate, chain, sslPolicyErrors) => { return true; };
            }

            byte[] dataToEncode;
            Encoding enc = Encoding.UTF8;
            dataToEncode = enc.GetBytes(data);
            WebClientExtension wc = new WebClientExtension();

            wc.Headers.Add("Content-Type", contentType);
            byte[] response = wc.UploadData(url, "POST", dataToEncode);
            string result = enc.GetString(response);
            return result;
        }
        public string Get(string url)
        {
            return "";
        }

        public Config LoadConfig()
        {
            try
            {
                Config Config = LoadConfig(ConfigLocation);
                return Config;

            }
            catch (Exception ex)
            {

                AutomateThisEvents.EventErrorLog(MessageFormatterException("Util.LoadConfig()", ex), Config.ErrorLogLocation);
                return null;
            }

        }
        public static Config LoadConfig(string configLocation)
        {
            try
            {
                using (StreamReader streamReader = File.OpenText(configLocation))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    Config Config = (Config)serializer.Deserialize(streamReader, typeof(Config));
                    return Config;
                }
            }
            catch
            {
                return null;
            }

        }
        public EnvironmentVars LoadEnvironmentVars()
        {
            try
            {
                using (StreamReader streamReader = File.OpenText(EnvironmentVarsLocation))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    EnvironmentVars environmentVars = (EnvironmentVars)serializer.Deserialize(streamReader, typeof(EnvironmentVars));
                    return environmentVars;
                }
            }
            catch (Exception ex)
            {

                AutomateThisEvents.EventErrorLog(MessageFormatterException("Util.LoadEnvironmentVars()", ex), Config.ErrorLogLocation);
                return null;
            }

        }


        public List<PageConfig> LoadPageConfigs()
        {
            string currentFile = "";
            try
            {
                List<PageConfig> pageConfigs = new List<PageConfig>();
                var DirectoryFiles = Directory.GetFiles(PageConfigLocation, "*.json", SearchOption.AllDirectories);
                foreach (string file in DirectoryFiles)
                {
                    currentFile = file;
                    using (StreamReader streamReader = File.OpenText(file))
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        PageConfig pageConfig = (PageConfig)serializer.Deserialize(streamReader, typeof(PageConfig));
                        pageConfig.FileName = currentFile;
                        PageConfig existingPageConfig = GetPageConfigById(pageConfigs, pageConfig.PageConfigId);
                        if (existingPageConfig == null)
                            pageConfigs.Add(pageConfig);
                        else
                            throw new Exception(string.Format("There is already a file with this PageConfigId:{0},\n Current File Name: {1}\n Existing filename: {2}", pageConfig.PageConfigId, currentFile, existingPageConfig.FileName));

                    }

                }

                return pageConfigs;
            }
            catch (Exception ex)
            {
                AutomateThisEvents.EventErrorLog(MessageFormatterException("Util.LoadPageConfigs() Filename: " + currentFile, ex), Config.ErrorLogLocation);
                return null;
            }

        }

        public string SerializeObject(object obj)
        {
            string result = "";
            try
            {
                result = JsonConvert.SerializeObject(obj, Formatting.Indented, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore

                });
            }
            catch (JsonSerializationException ex)
            {
                AutomateThisEvents.EventErrorLog(MessageFormatterException("Util.SerializeObject()", ex), Config.ErrorLogLocation);
            }

            return result;
        }

        public string GetTokenValue(List<PageConfig> pageConfigs, string sKey)
        {
            string sOutput = sKey;
            try
            {
                if (sKey != null)
                {
                    if (sKey.Length > 3)
                    {
                        if (sKey.Contains("${") && sKey.Contains("}"))
                        {
                            int beginIn = sKey.IndexOf("${") + 2;
                            int endIn = sKey.IndexOf("}");
                            if (endIn > (beginIn + 1))
                            {
                                string sKey1 = sKey.Substring(beginIn, endIn - beginIn);
                                string sValue = null;

                                if (sKey1.Contains(" "))
                                {
                                    sKey1 = sKey1.Replace(" ", "");
                                }

                                if (sKey1.Contains("."))
                                {
                                    int indexcomma = sKey1.IndexOf(".");
                                    string sPageConfigId = sKey1.Substring(0, indexcomma);
                                    string sKey2 = sKey1.Substring(indexcomma + 1);
                                    PageConfig exists = GetPageConfigById(pageConfigs, sPageConfigId);
                                    if (exists == null)
                                        throw new Exception(string.Format("PageConfigId:{0} wasn't found for token:{1}", sPageConfigId, sKey));

                                    sValue = GetPageConfigValueById(pageConfigs, sPageConfigId, sKey2);
                                    if (sValue == null)
                                        throw new Exception(string.Format("A value wasn't found for token:{0} using the following file:{1}", sKey2, exists.FileName));

                                }
                                else
                                {
                                    sKey = sKey.Replace("${", "");
                                    sKey = sKey.Replace("}", "");
                                    int start = sKey.IndexOf("#");
                                    string value = sKey.Substring(start + 1);
                                    sKey = sKey.Replace(value, "");
                                    sKey = sKey.Replace("#", "");
                                    DoCommands(sKey, value);
                                    sOutput = CustomString;
                                    return sOutput;
                                }

                                if (sValue != null)
                                {
                                    sOutput = sKey.Replace("${" + sKey1 + "}", sValue);
                                }
                            }
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                AutomateThisEvents.EventErrorLog(MessageFormatterException("Util.GetTokenValue()", ex), Config.ErrorLogLocation);
            }
            return sOutput;
        }
        public string GetSavedValue(List<PageConfig> pageConfigs, string sKey)
        {
            string sOutput = sKey;
            try
            {
                if (sKey != null)
                {
                    if (sKey.Length > 3)
                    {
                        if (sKey.Contains("#{") && sKey.Contains("}"))
                        {
                            int beginIn = sKey.IndexOf("#{") + 2;
                            int endIn = sKey.IndexOf("}");
                            if (endIn > (beginIn + 1))
                            {
                                string sKey1 = sKey.Substring(beginIn, endIn - beginIn);
                                if (sKey1.Contains(" "))
                                {
                                    sKey1 = sKey1.Replace(" ", "");
                                }

                                if (sKey1.Contains("$"))
                                {
                                    int index = sKey1.IndexOf("$");
                                    string sPageConfigId = sKey1.Substring(0, index);
                                    string sKey2 = sKey1.Substring(index + 1);
                                    var pageConfig = GetPageConfigById(pageConfigs, sPageConfigId);
                                    if (pageConfig == null)
                                        throw new Exception(string.Format("PageConfigId:{0} wasn't found for token:{1}", sPageConfigId, sKey));

                                    sOutput = QueryJsonString(pageConfig.SavedValue, sKey2);
                                    if (sOutput == null)
                                        throw new Exception(string.Format("A value wasn't found for token:{0} using the following file:{1}", sKey, pageConfig.FileName));

                                }

                            }
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                AutomateThisEvents.EventErrorLog(MessageFormatterException("Util.GetSavedValue()", ex), Config.ErrorLogLocation);
            }
            return sOutput;
        }
        public dynamic GetPageConfigSavedValue(List<PageConfig> pageConfigs, string id)
        {
            try
            {
                var pageConfig = GetPageConfigById(pageConfigs, id);
                return pageConfig.SavedValue;
            }
            catch (Exception ex)
            {
                AutomateThisEvents.EventErrorLog(MessageFormatterException("Util.GetPageConfigSavedValue()", ex), Config.ErrorLogLocation);
                return null;
            }

        }
        public void SetPageConfigSavedValue(List<PageConfig> pageConfigs, string id, string data)
        {
            try
            {
                var pageConfig = GetPageConfigById(pageConfigs, id);
                pageConfig.SavedValue = data;
            }
            catch (Exception ex)
            {
                AutomateThisEvents.EventErrorLog(MessageFormatterException("Util.SetPageConfigSavedValue()", ex), Config.ErrorLogLocation);
            }
        }
        public PageConfig GetPageConfigById(List<PageConfig> pageConfigs, string id)
        {
            try
            {
                var pageConfig = pageConfigs.SingleOrDefault(p => p.PageConfigId == id);
                return pageConfig;
            }
            catch (Exception ex)
            {
                AutomateThisEvents.EventErrorLog(MessageFormatterException("Util.GetPageConfigById()", ex), Config.ErrorLogLocation);
                return null;
            }

        }
        public string QueryJsonString(dynamic obj, string query)
        {
            try
            {
             
                JToken token = JToken.Parse(obj);
                string result = (string )token.SelectToken(query);
                //string result = (string)root.SelectToken(query);
                return result;
            }
            catch (Exception ex)
            {
                AutomateThisEvents.EventErrorLog(MessageFormatterException("Util.QueryJsonString()", ex), Config.ErrorLogLocation);
                return null;
            }


        }
        public string GetPageConfigValueById(List<PageConfig> pageConfigs, string PageConfigId, string key)
        {
            try
            {
                var pageConfig = GetPageConfigById(pageConfigs, PageConfigId);
                if (pageConfig.SavedValues.ContainsKey(key))
                {
                    return pageConfig.SavedValues[key];
                }

            }
            catch (Exception ex)
            {
                AutomateThisEvents.EventErrorLog(MessageFormatterException("Util.GetPageConfigValueById()", ex), Config.ErrorLogLocation);
            }
            return null;
        }
        string DoCommands(string cmd, string value)
        {
            try
            {
                Commands[cmd].Invoke(Convert.ToInt32(value));
            }
            catch (Exception ex)
            {
                AutomateThisEvents.EventErrorLog(MessageFormatterException("Util.DoCommands()", ex), Config.ErrorLogLocation);
            }
            return CustomString;
        }
        void AddCommands()
        {
            Commands.Add("random_number", GenerateRandomNumber);
            Commands.Add("random_amount", GenerateRandomAmount);
            Commands.Add("random_value", GenerateRandomValueByLength);

        }

        void GenerateRandomNumber(int length)
        {
            Random random = new Random();
            int value = random.Next(0, length);

            CustomString = value.ToString();
        }
        public int GenerateRandomNumber(int start, int end)
        {
            Random random = new Random();
            int value = random.Next(start, end);
            return value;
        }

        void GenerateRandomAmount(int length)
        {
            Random random = new Random();
            int dollar = random.Next(1, length);
            int num2 = random.Next(0, 99);

            if (num2 <= 9)
            {
                CustomString = dollar + "." + 0 + num2;

                return;
            }
            CustomString = dollar + "." + num2;
        }
        void GenerateTimeStamp()
        {
            CustomString = GetTimeStamp();
        }

        void GenerateRandomValueByLength(int length)
        {
            GenerateRandomValue(length);
        }


        void GenerateRandomValue(int size)
        {
            using (RandomNumberGenerator rng = new RNGCryptoServiceProvider())
            {
                byte[] tokenData = new byte[size];
                rng.GetBytes(tokenData);

                CustomString = Convert.ToBase64String(tokenData);

            }
        }
        public string[] CreateStringArrayFromCSV(string value)
        {
            //Regex.Split(value,",");

            return value.Split(new string[] { "," }, StringSplitOptions.None);
        }
        public string ReplaceString(string startPattern, string endPattern, string value)
        {

            return value;
        }
        public string[] ReplaceTokensInCSV(string value, List<PageConfig> pageConfigs)
        {
            string[] parameters = CreateStringArrayFromCSV(value);
            int i = 0;
            foreach (string str in parameters)
            {

                if (str != null && str.Contains("${") && str.Contains("}"))
                {
                    parameters[i] = GetTokenValue(pageConfigs, str);

                }
                else if (str != null && str.Contains("#{") && str.Contains("}"))
                {
                    parameters[i] = GetSavedValue(pageConfigs, str);
                }
                i++;
            }
            return parameters;
        }


    }


}
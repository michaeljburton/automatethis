using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using AutomateThis.Protractor;
using AutomateThis.Model;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;

namespace AutomateThis
{
    public class TestRunner
    {
        public WebDriverWait Wait;
        public List<Project> CurrentProjects;
        public Project CurrentProject;
        public Feature CurrentFeature;
        public TestSuite CurrentTestSuite;
        public Testcase CurrentTestCase;
        public TestRunResult CurrentTestRunResult;
        public TestSuiteResult CurrentTestSuiteResult;
        public TestCaseResult CurrentTestCaseResult;
        public Repository Repository;
        Commands Cmds;
        Dictionary<string, Action> Commands;
        Dictionary<string, Action> SubCommands;
        AutomateThisDB DB;
        public TestRunner(Repository repository)
        {
            this.Repository = repository;
            this.DB = new AutomateThisDB(Repository);
            this.Commands = new Dictionary<string, Action>();
            this.SubCommands = new Dictionary<string, Action>();
            LoadCommands();
        }
        public void Run(Commands commands)
        {
            Cmds = commands;
            ///LoadWebDriver(driver);
            // cmds[0] = Browser , cmd[1] = cmd ,cmd[2] = Query , cmd[3] = environment , cmd[4] = country 

            //TODO Validate cmds      
            try
            {
                if (Repository.PageConfigs == null)
                    return;
                LoadWebDriver(Cmds.WebDriver);
                Repository.WebDriverWrapper = new WebDriverWrapper(Repository);
                Repository.TestRunLocation = Repository.Util.Config.TestResultLocation + "\\TestRun\\";
                Directory.CreateDirectory(Repository.TestRunLocation);
                for (int i = 0; i < Cmds.Repeat; i++)
                    Commands[Cmds.Command].Invoke();

            }
            catch (Exception ex)
            {
                Repository.AutomateThisEvents.EventErrorLog(Repository.Util.MessageFormatterException("TestRunner.Run()", ex), Repository.Util.Config.ErrorLogLocation);
            }
        }
        void LoadCommands()
        {
            // load main commands 
            Commands.Add("ra", RunAll);// Run all testsuites in all projects
            Commands.Add("pra", RunAllProjectTestSuites);//Run all testsuites in queryed project e.g. ProjectName
            Commands.Add("fra", RunAllFeatureTestSuites);//Run all testsuites in queryed feature e.g. ProjectName.FeatureName
            Commands.Add("rts", RunTestSuite);// Run queryed testsuite e.g.ProjectName.FeatureName.TestSuiteName
            Commands.Add("rtc", RunTestCase);// Run queryed testcase e.g. ProjectName.FeatureName.TestSuiteName.TestCaseName
            //load sub Commands 
            SubCommands.Add("firefox", LoadFireFoxDriver);
            SubCommands.Add("edge", LoadEdgeDriver);
            SubCommands.Add("ie", LoadIEDriver);
            SubCommands.Add("chrome", LoadChromeDriver);
        }
        void LoadWebDriver(string driver)
        {
            try
            {
                SubCommands[driver].Invoke();
            }
            catch (Exception ex)
            {
                Repository.AutomateThisEvents.EventErrorLog(Repository.Util.MessageFormatterException("TestRunner.LoadWebDriver()", ex), Repository.Util.Config.ErrorLogLocation);
            }
        }
        void LoadFireFoxDriver()
        {
            FirefoxOptions FirefoxOptions = new FirefoxOptions();
            DesiredCapabilities capability = (DesiredCapabilities)FirefoxOptions.ToCapabilities();
            if (Repository.Util.Config.EnableRemoteDriver > 0)
            {
                Repository.WebDriver = new RemoteWebDriver(new Uri(Repository.Util.Config.RemoteDriverUrl), capability);
            }
            else
            {
                Repository.WebDriver = new FirefoxDriver();
            }

            Wait = new WebDriverWait(Repository.WebDriver, TimeSpan.FromSeconds(Repository.Util.Config.SeleniumWaitTime));
            Repository.WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(Repository.Util.Config.Timeout);
            Repository.WebDriver.Manage().Window.Maximize();
            Repository.NgWebDriver = new NgWebDriver(Repository.WebDriver);

        }
        void LoadChromeDriver()
        {

            var chromeOptions = new ChromeOptions();
            chromeOptions.Proxy = null;
            chromeOptions.AddArgument("--disable-gpu");
            chromeOptions.AddArgument("--start-maximized");
            chromeOptions.AddArgument("--ignore-certificate-errors");
            chromeOptions.AddArgument("--disable-popup-blocking");
            DesiredCapabilities capability = (DesiredCapabilities)chromeOptions.ToCapabilities();
            if (Repository.Util.Config.EnableRemoteDriver > 0)
            {
                capability.SetCapability("platform", "WINDOWS");
                Repository.WebDriver = new RemoteWebDriver(new Uri(Repository.Util.Config.RemoteDriverUrl), capability);

            }
            else
            {
                Repository.WebDriver = new ChromeDriver(AppDomain.CurrentDomain.BaseDirectory, chromeOptions);
            }
            Wait = new WebDriverWait(Repository.WebDriver, TimeSpan.FromSeconds(Repository.Util.Config.SeleniumWaitTime));
            Repository.WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(Repository.Util.Config.Timeout);
            Repository.NgWebDriver = new NgWebDriver(Repository.WebDriver);
        }
        void LoadIEDriver()
        {
            var options = new InternetExplorerOptions
            {
                IgnoreZoomLevel = true,
                RequireWindowFocus = true,
                AcceptInsecureCertificates = true,
                IntroduceInstabilityByIgnoringProtectedModeSettings = true

            };

            Repository.WebDriver = new InternetExplorerDriver(AppDomain.CurrentDomain.BaseDirectory, options);
            Repository.NgWebDriver = new NgWebDriver(Repository.WebDriver);
        }
        void LoadEdgeDriver()
        {
            Repository.WebDriver = new EdgeDriver(AppDomain.CurrentDomain.BaseDirectory);
            Repository.WebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(Repository.Util.Config.Timeout);
            Repository.NgWebDriver = new NgWebDriver(Repository.WebDriver);
        }
        void RunAll()
        {
            //load projects
            CurrentProjects = DB.GetAllProjects();
            if (CurrentProjects == null)
            {
                Repository.AutomateThisEvents.EventErrorLog(Repository.Util.MessageFormatter("No Projects found!"), Repository.Util.Config.ErrorLogLocation);
                return;
            }
            TestRunResult TestRunResult = new TestRunResult
            {
                TimeStamp = Repository.Util.GetTimeStamp()
            };
            CreateLogLocation();
            LoopProjects(CurrentProjects);
            Repository.AutomateThisEvents.EventTestRunResults(TestRunResult, Repository.TestRunLocation);
        }
        void RunTestSuite()
        {
            CurrentTestSuite = DB.GetTestSuite(Cmds.Query);
            if (CurrentTestSuite == null)
            {
                Repository.AutomateThisEvents.EventErrorLog(Repository.Util.MessageFormatter("No test suite found!"), Repository.Util.Config.ErrorLogLocation);
                return;
            }
            CurrentTestRunResult = new TestRunResult
            {
                TimeStamp = Repository.Util.GetTimeStamp()
            };

            CreateLogLocation();
            LoopTestsuite(CurrentTestSuite);
            Repository.AutomateThisEvents.EventTestRunResults(CurrentTestSuiteResult, Repository.TestRunLocation);
        }

        void RunTestCase()
        {

            CurrentTestCase = DB.GetTestCase(Cmds.Query);
            if (CurrentTestCase == null)
            {
                Repository.AutomateThisEvents.EventErrorLog(Repository.Util.MessageFormatter("Test Case not found!"), Repository.Util.Config.ErrorLogLocation);
                return;
            }
            CreateLogLocation();
            LoopTestCase(CurrentTestCase);
            Repository.AutomateThisEvents.EventTestRunResults(CurrentTestCaseResult, Repository.TestRunLocation);
        }
        void RunAllProjectTestSuites()
        {

            CurrentProject = DB.GetProjectTestSuites(Cmds.Query);
            if (CurrentProject == null)
            {
                Repository.AutomateThisEvents.EventErrorLog(Repository.Util.MessageFormatter("No Projects found!"), Repository.Util.Config.ErrorLogLocation);
                return;
            }
            CurrentTestRunResult = new TestRunResult
            {
                TimeStamp = Repository.Util.GetTimeStamp()
            };
            CreateLogLocation();
            LoopProject(CurrentProject);
            Repository.AutomateThisEvents.EventTestRunResults(CurrentTestRunResult, Repository.TestRunLocation);

        }
        void RunAllFeatureTestSuites()
        {
            CurrentFeature = DB.GetFeaturesTestSuites(Cmds.Query);
            if (CurrentFeature == null)
            {
                Repository.AutomateThisEvents.EventErrorLog(Repository.Util.MessageFormatter("No Feature found!"), Repository.Util.Config.ErrorLogLocation);
                return;
            }
            CurrentTestRunResult = new TestRunResult
            {
                TimeStamp = Repository.Util.GetTimeStamp()
            };
            CreateLogLocation();
            LoopFeature(CurrentFeature);
            Repository.AutomateThisEvents.EventTestRunResults(CurrentTestRunResult, Repository.TestRunLocation);
        }
        public List<Step> SortSteps(List<Step> stepsToSort)
        {
            var steps = from s in stepsToSort orderby s.Sequence ascending select s;
            return steps.ToList();
        }
        public void LoopProjects(List<Project> projects)
        {
            foreach (var project in projects)
                LoopProject(project);

        }
        public void LoopProject(Project project)
        {
            Repository.AutomateThisEvents.EventLog(Repository.Util.MessageFormatter(string.Format("Project: {0} has Started", project.Id), Repository.MessageType.Result), Repository.CurrentLogFileLocation);
            Repository.CurrentProjectName = project.Id;
            foreach (var feature in project.Features)
                LoopFeature(feature);
            Repository.AutomateThisEvents.EventLog(Repository.Util.MessageFormatter(string.Format("Project: {0} has finished", project.Id), Repository.MessageType.Result), Repository.CurrentLogFileLocation);
        }
        public void LoopFeature(Feature feature)
        {
            Repository.AutomateThisEvents.EventLog(Repository.Util.MessageFormatter(string.Format("Feature: {0} has Started", feature.Id), Repository.MessageType.Result), Repository.CurrentLogFileLocation);
            Repository.CurrentFeatureName = feature.Id;
            foreach (var testsuite in feature.TestSuites)
                LoopTestsuite(testsuite);
            Repository.AutomateThisEvents.EventLog(Repository.Util.MessageFormatter(string.Format("Feature: {0} has finished", feature.Id), Repository.MessageType.Result), Repository.CurrentLogFileLocation);
        }
        public void LoopTestsuite(TestSuite testsuite)
        {
            Repository.AutomateThisEvents.EventLog(Repository.Util.MessageFormatter(string.Format("Testsuite: {0} has Started", testsuite.Id), Repository.MessageType.Result), Repository.CurrentLogFileLocation);
            Repository.CurrentTestsuite = testsuite.Id;
            CurrentTestSuiteResult = new TestSuiteResult
            {
                TestSuiteId = testsuite.Id,
            };
            if (testsuite.Testcases != null)
            {
                Stopwatch testsuiteStopWatch = new Stopwatch();
                testsuiteStopWatch.Start();
                foreach (var testCase in testsuite.Testcases)
                    LoopTestCase(testCase);
                Repository.AutomateThisEvents.EventLog(Repository.Util.MessageFormatter(string.Format("Testsuite: {0} has Finished", testsuite.Id), Repository.MessageType.Result), Repository.CurrentLogFileLocation);
                testsuiteStopWatch.Stop();
                TimeSpan testsuiteTimeSpan = testsuiteStopWatch.Elapsed;
                CurrentTestSuiteResult.ExecutionTime = Repository.Util.FormatTimeSpan(testsuiteTimeSpan);
                if (CurrentTestRunResult != null)
                {
                    CurrentTestRunResult.TestSuiteResults.Add(CurrentTestSuiteResult);
                }

            }
        }
        public void LoopTestCase(Testcase testCase)
        {
            TimeSpan testcaseTimeSpan;
            Stopwatch testcaseStopWatch = new Stopwatch();
            testcaseStopWatch.Start();
            if (testCase.Steps != null)
            {

                CurrentTestCaseResult = new TestCaseResult
                {
                    TestCaseId = testCase.Id,

                    Result = true,
                    Message = "Test case Passed!"

                };
                Repository.CurrentTestCase = testCase.Id;

                Repository.AutomateThisEvents.EventLog(Repository.Util.MessageFormatter(string.Format("Test Case: {0} has Started", testCase.Id), Repository.MessageType.Result), Repository.CurrentLogFileLocation);
                foreach (var step in SortSteps(testCase.Steps))
                {
                    Stopwatch stepStopWatch = new Stopwatch();
                    stepStopWatch.Start();
                    StepRunner stepRunner = new StepRunner();
                    var stepResult = stepRunner.Run(step, Repository);
                    stepStopWatch.Stop();
                    TimeSpan stepTimeSpan = stepStopWatch.Elapsed;
                    stepResult.ExecutionTime = Repository.Util.FormatTimeSpan(stepTimeSpan);
                    CurrentTestCaseResult.StepResults.Add(stepResult);
                    Repository.AutomateThisEvents.EventLog(Repository.Util.MessageFormatter(stepResult, Repository.MessageType.Result), Repository.CurrentLogFileLocation);
                    if (!stepResult.Result)
                    {
                        if (stepResult.Conditional)
                        {
                            CurrentTestCaseResult.Message = string.Format("Test case passed but a conditional step failed see: StepId {0}", step.Id);
                            CurrentTestCaseResult.Result = true;
                        }
                        else
                        {
                            CurrentTestCaseResult.Message = string.Format("Test case failed see StepId {0}", step.Id);
                            CurrentTestCaseResult.Result = false;
                            break;
                        }

                    }


                }
                Repository.AutomateThisEvents.EventLog(Repository.Util.MessageFormatter(string.Format("Test Case: {0} has finished with the the following result: {1}", testCase.Id, CurrentTestCaseResult.Message), Repository.MessageType.Result), Repository.CurrentLogFileLocation);
                testcaseStopWatch.Stop();
                testcaseTimeSpan = testcaseStopWatch.Elapsed;
                CurrentTestCaseResult.ExecutionTime = Repository.Util.FormatTimeSpan(testcaseTimeSpan);
                if (CurrentTestSuiteResult != null)
                {
                    CurrentTestSuiteResult.TestCaseResults.Add(CurrentTestCaseResult);
                }

            }

        }
        public void CreateLogLocation()
        {
            Repository.CurrentLogFileLocation = Repository.TestRunLocation + "FullLog_" + Cmds.WebDriver + "_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".log";
        }
    }

}
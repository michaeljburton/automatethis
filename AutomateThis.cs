using System;
using System.IO;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using AutomateThis.Model;


namespace AutomateThis
{
    public class AutomateThis : Main, IAutomateThisEvents
    {
        const string ENV_CMD = "-env=";
        const string SUBENV_CMD = "-subenv=";
        const string WEBDRIVER_CMD = "-webdriver=";
        const string CMD = "-cmd=";
        const string QUERY = "-query=";
        const string REPEAT = "-repeat=";
        Config Config;
        Commands CurrentCommands;
        public Repository Repository;
        public AutomateThis()
        {



        }
        public void Run(string[] args)
        {


            //validate args
            if (IsValidArgs(args))
            {
                string cmdString = "";
                for (int i = 0; i < args.Length; i++)
                    cmdString += args[i] + "\n";
                //create a Commands object and assign values
                CurrentCommands = new Commands
                {
                    Command = FindCmdReturnsStringValue(CMD, cmdString),
                    Environment = FindCmdReturnsStringValue(ENV_CMD, cmdString),
                    WebDriver = FindCmdReturnsStringValue(WEBDRIVER_CMD, cmdString),
                    SubEnvironment = FindCmdReturnsStringValue(SUBENV_CMD, cmdString),
                    Query = FindCmdReturnsStringValue(QUERY, cmdString),
                    Repeat = FindCmdReturnsIntValue(REPEAT, cmdString)
                };
                //call automatethis.run and pass in a object of Command 
                //AutomateThis = new AutomateThis(this, CurrentCommands);
                Config = Util.LoadConfig(Util.ConfigLocation);
                Repository = new Repository(this, CurrentCommands,Config);
                Directory.CreateDirectory(Config.ErrorLogLocation);
                TestRunner testRunner = new TestRunner(Repository);
                testRunner.Run(CurrentCommands);
                // AutomateThis.Run();


            }

        }
        public void EventErrorLog(string message, string currentErrorLogLocation)
        {
            Console.WriteLine(message);
            if (currentErrorLogLocation != "")
            {
                Log(message, currentErrorLogLocation + "\\" + "Errors.log");
            }
        }

        public void EventLog(string message, string currentLogLocation)
        {
            Console.WriteLine(message);
            if (currentLogLocation != "")
            {
                Log(message, currentLogLocation);
            }

        }
        public void EventLog(string message)
        {
            Console.WriteLine(message);
        }
        public void EventTestRunResults(object obj, string currentLogLocation)
        {
            var message = ToJSON(obj, NullValueHandling.Ignore, ReferenceLoopHandling.Ignore, Formatting.Indented);
            Console.WriteLine(message);
            if (currentLogLocation != "")
            {
                Log(message, currentLogLocation + "Result_" + CurrentCommands.WebDriver + "_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".json");
                Log(WriteCsv(obj), currentLogLocation + "Report_" + CurrentCommands.WebDriver + "_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".csv");
            }


        }
        void Log(string message, string fileLocation)
        {
            StreamWriter StreamWriter1;

            if (Config.EnableLogs > 0)
            {
                using (StreamWriter1 = File.AppendText(fileLocation))
                {
                    StreamWriter1.WriteLine(message);
                    StreamWriter1.Close();
                }

            }
        }
        bool IsValidArgs(string[] args)
        {
            if (args == null)
            {
                return false;
            }
            if (args.Length <= 3)
            {
                return false;
            }

            return true;
        }
        string FindCmdReturnsStringValue(string cmd, string input)
        {
            string value = "";
            string pattern = cmd + "'.*'";
            Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            MatchCollection matches = rgx.Matches(input);
            if (matches != null)
            {
                if (matches.Count > 0)
                {
                    Match match = (Match)matches[0];
                    value = match.Value;
                    int pFrom = value.IndexOf("'") + "'".Length;
                    int pTo = value.LastIndexOf("'");
                    value = value.Substring(pFrom, pTo - pFrom);
                }
            }
            return value;

        }
        int FindCmdReturnsIntValue(string cmd, string input)
        {
            string value = FindCmdReturnsStringValue(cmd, input);
            int result;
            bool isNumeric = int.TryParse(value, out result);
            if (isNumeric)
                return result;
            return 1;
        }
        string WriteCsv(object obj)
        {
            string result = "";

            if (obj != null)
            {
                var type = obj.GetType();
                if (type == typeof(TestCaseResult))
                {
                    TestCaseResult testCaseResult = (TestCaseResult)obj;
                    result = "TestCaseId,StepId,StepExecutionTime,StepElementReturnedTime,StepBehaviourReturnedTime\n";
                    foreach (var step in testCaseResult.StepResults)
                    {
                        result += string.Format("{0},{1},{2},{3},{4}\n", testCaseResult.TestCaseId, step.StepId,
                        step.ExecutionTime, step.ElementReturnedTime, step.BehaviourReturnedTime);

                    }

                }
                else if (type == typeof(TestSuiteResult))
                {
                    TestSuiteResult testSuiteResult = (TestSuiteResult)obj;
                    result = "TestSuiteId,TestCaseId,StepId,StepExecutionTime,StepElementReturnedTime,StepBehaviourReturnedTime\n";
                    foreach (var testCase in testSuiteResult.TestCaseResults)
                    {
                        foreach (var step in testCase.StepResults)
                        {
                            result += string.Format("{0},{1},{2},{3},{4},{5}\n", testSuiteResult.TestSuiteId, testCase.TestCaseId, step.StepId,
                            step.ExecutionTime, step.ElementReturnedTime, step.BehaviourReturnedTime);

                        }
                    }

                }
                else if (type == typeof(TestRunResult))
                {
                    TestRunResult testRunResult = (TestRunResult)obj;
                    result = "TestSuiteId,TestCaseId,StepId,StepExecutionTime,StepElementReturnedTime,StepBehaviourReturnedTime\n";
                    foreach (var testSuiteResult in testRunResult.TestSuiteResults)
                    {
                        foreach (var testCase in testSuiteResult.TestCaseResults)
                        {
                            foreach (var step in testCase.StepResults)
                            {
                                result += string.Format("{0},{1},{2},{3},{4},{5}\n", testSuiteResult.TestSuiteId, testCase.TestCaseId, step.StepId,
                                step.ExecutionTime, step.ElementReturnedTime, step.BehaviourReturnedTime);

                            }
                        }
                    }

                }
            }
            return result;
        }


    }
}
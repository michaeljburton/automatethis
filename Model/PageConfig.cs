using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomateThis.Model
{
     public class PageConfig
    {
        public string PageConfigId { get; set; }
        public Dictionary<string, string> SavedValues = new Dictionary<string, string>();
        public dynamic SavedValue;
        public string FileName {get;set;}
        public PageConfig()
        {

        }
    }

}
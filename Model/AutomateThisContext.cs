using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;

namespace AutomateThis.Model
{
    public class AutomateThisContext : DbContext
    {
        public string DataBaseName = "automateThis.db";
        public string ConnectionString;
        public DbSet<Step> Steps { get; set; }
        public DbSet<Testcase> Testcases { get; set; }
        public DbSet<TestSuite> TestSuites { get; set; }
        public DbSet<Feature> Features { get; set; }
        public DbSet<Project> Projects { get; set; }

       
        public AutomateThisContext(string connectionString){
             if(connectionString != "")
                this.ConnectionString = connectionString;
             else
                ConnectionString = AppDomain.CurrentDomain.BaseDirectory + DataBaseName;
        }
        public AutomateThisContext(){
           
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source="+ConnectionString);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }

    }


}
using System;
namespace AutomateThis.Model
{
    public class Config
    {
        public string ErrorLogLocation { get; set; }
        public string TestResultLocation { get; set; }
        public byte SkipSslCheck { get; set; }
        public byte EnableLogs { get; set; }
        public byte EnableScreenShots { get; set; }
        public int Timeout { get; set; }
        public string DBLocation { get; set; }
        public string DBName { get; set; }
        public string RemoteDriverUrl { get; set; }
        public byte EnableRemoteDriver { get; set; }
        public int SeleniumWaitTime { get; set; }
        public long MaxPageLoadTime { get; set; }
        public Config()
        {
        }

    }
}
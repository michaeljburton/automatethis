using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace AutomateThis.Model
{
    public class Main
    {
        public IAutomateThisEvents AutomateThisEvents;
        public Main(IAutomateThisEvents automateThisEvents)
        {
            this.AutomateThisEvents = automateThisEvents;
        }
        public Main() { }
        //Clone Object
        public T Clone<T>(T source)
        {
            var serialized = JsonConvert.SerializeObject(source);
            return JsonConvert.DeserializeObject<T>(serialized);
        }

        public string ToXML<T>(T value, System.Xml.Formatting formatting)
        {
            if (value == null)
            {
                return string.Empty;
            }
            var xmlserializer = new XmlSerializer(typeof(T));
            var stringWriter = new StringWriter();
            XmlTextWriter writer = new XmlTextWriter(stringWriter)
            {
                Formatting = formatting
            };

            xmlserializer.Serialize(writer, value);
            return stringWriter.ToString();
        }
        public string ToJSON(object obj, NullValueHandling nullValueHandling, ReferenceLoopHandling referenceLoopHandling, Newtonsoft.Json.Formatting formatting)
        {
            string json = "";
            json = JsonConvert.SerializeObject(obj, new JsonSerializerSettings
            {
                NullValueHandling = nullValueHandling,
                ReferenceLoopHandling = referenceLoopHandling,
                Formatting = formatting

            });
            return json;
        }
        public T GetObjectFromJSON<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);

        }
        public object GetObjectFromJSONFile(StreamReader streamReader, Type type)
        {
            JsonSerializer serializer = new JsonSerializer();
            return serializer.Deserialize(streamReader, type);

        }
        public T GetObjectFromXML<T>(string xml)
        {
            var xmlserializer = new XmlSerializer(typeof(T));
            var stringWriter = new StringReader(xml);
            return (T)xmlserializer.Deserialize(stringWriter);

        }


        public string PrettyXml(string xml)
        {
            xml = System.Xml.Linq.XElement.Parse(xml).ToString();

            return xml;
        }
    }
    public class Step : Main
    {
        //[Key]
        public int Id { get; set; }
        public string TestCaseId { get; set; }
        public Testcase Testcase { get; set; }
        public string Command { get; set; }
        public string By { get; set; }
        public string Element { get; set; }
        public string Input { get; set; }
        public bool Conditional { get; set; }
        public int MessageFormat { get; set; }
        public int Size { get; set; }
        public int Delay { get; set; }
        public string Data {get;set;}
        public string Description { get; set; }
        public int Sequence { get; set; }
        public string CustomCommand { get; set; }
        public string CustomFunction { get; set; }
        public string CustomParameters { get; set; }
        
        public Step() { }
    }
    public class Testcase : Main
    {
        //[Key]
        public string Id { get; set; }
        public string TestSuiteId { get; set; }
        public TestSuite TestSuite { get; set; }
        public string Description { get; set; }
        public List<Step> Steps { get; set; }
        public Testcase()
        {
            Steps = new List<Step>();
        }
    }

    public class TestSuite : Main
    {
        //[Key]
        public string Id { get; set; }
        public string FeatureId { get; set; }
        public Feature Feature { get; set; }
        public string Description { get; set; }
        public List<Testcase> Testcases { get; set; }
        public TestSuite()
        {
            Testcases = new List<Testcase>();
        }
    }

    public class Feature : Main
    {
        public string Id { get; set; }
        public string ProjectId { get; set; }
        public Project Project { get; set; }
        public string Description { get; set; }
        public List<TestSuite> TestSuites { get; set; }
        public Feature()
        {
            TestSuites = new List<TestSuite>();
        }

    }
    public class Project : Main
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public List<Feature> Features { get; set; }
        public Project()
        {
            Features = new List<Feature>();
        }

    }
    public class StepResult : Main
    {
        public int StepId { get; set; }
        public bool Conditional { get; set; }
        public string Message { get; set; }
        public Exception Exception { get; set; }
        public bool Result { get; set; }
        public string ExecutionTime {get;set;}
        public string ElementReturnedTime {get;set;}
        public string BehaviourReturnedTime {get;set;}
        public string Command {get;set;}
        public string By {get;set;}
        public StepResult() { }

    }
    public class TestCaseResult : Main
    {
        public string TestCaseId { get; set; }
        public string Message { get; set; }
        public bool Result { get; set; }
        public string  ExecutionTime {get;set;}
        public List<StepResult> StepResults = new List<StepResult>();
        public TestCaseResult() { }
    }
    public class TestSuiteResult : Main
    {
        public string TestSuiteId { get; set; }
        public string  ExecutionTime {get;set;}
        public List<TestCaseResult> TestCaseResults = new List<TestCaseResult>();
    }
    public class TestRunResult : Main
    {
        public string TimeStamp { set; get; }
        public List<TestSuiteResult> TestSuiteResults = new List<TestSuiteResult>();
    }
    public class EnvironmentVars
    {
        public Dictionary<string, Dictionary<string, Dictionary<string,string>>> Environments = new Dictionary<string,  Dictionary<string, Dictionary<string,string>>>();       

    }
    public class Commands
    {
        public string Environment { get; set; }
        public string SubEnvironment { get; set; }
        public string WebDriver { get; set; }
        public string Query { get; set; }
        public string Command { get; set; }
        public int Repeat { get; set; }
    }


}

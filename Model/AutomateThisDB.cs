using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;
using System.Linq;

namespace AutomateThis.Model
{
    public class AutomateThisDB
    {
        public Repository Repository;
        string ConnectionString;

        public AutomateThisDB(Repository repository)
        {
            this.Repository = repository;
            ConnectionString = Repository.Util.Config.DBLocation + Repository.Util.Config.DBName;
        }
        public List<Project> GetAllProjects()
        {
            try
            {
                using (var db = new AutomateThisContext(ConnectionString))
                {
                    var projects = db.Projects
                    .Include(p => p.Features)
                         .ThenInclude(f => f.TestSuites)
                             .ThenInclude(ts => ts.Testcases)
                                 .ThenInclude(tc => tc.Steps).ToList();
                    return projects;

                }
            }
            catch (Exception ex)
            {
                Repository.AutomateThisEvents.EventErrorLog(string.Format("GetAllProjects() failed with following Exception: {0}", ex.Message), Repository.Util.Config.ErrorLogLocation);
            }
            return null;

        }
        public Project GetProjectTestSuites(string projectId)
        {
            try
            {
                using (var db = new AutomateThisContext(ConnectionString))
                {
                    var project = db.Projects
                    .Include(p => p.Features)
                         .ThenInclude(f => f.TestSuites)
                             .ThenInclude(ts => ts.Testcases)
                                 .ThenInclude(tc => tc.Steps)
                                    .SingleOrDefault(p => p.Id == projectId);
                    return project;
                }
            }
            catch (Exception ex)
            {
                Repository.AutomateThisEvents.EventErrorLog(string.Format("GetTestSuite() failed with following Exception: {0}", ex.Message), Repository.Util.Config.ErrorLogLocation);
            }
            return null;
        }
        public TestSuite GetTestSuite(string testSuiteId)
        {
            try
            {
                using (var db = new AutomateThisContext(ConnectionString))
                {
                    var testSuite = db.TestSuites
                        .Include(ts => ts.Testcases)
                            .ThenInclude(tc => tc.Steps)
                                .SingleOrDefault(t => t.Id == testSuiteId);
                    return testSuite;
                }
            }
            catch (Exception ex)
            {
                Repository.AutomateThisEvents.EventErrorLog(string.Format("GetTestSuite() failed with following Exception: {0}", ex.Message), Repository.Util.Config.ErrorLogLocation);
            }
            return null;

        }
        public Testcase GetTestCase(string testCaseId)
        {
            try
            {
                using (var db = new AutomateThisContext(ConnectionString))
                {
                    var testCase = db.Testcases
                        .Include(tc => tc.Steps)
                            .SingleOrDefault(t => t.Id == testCaseId);
                    return testCase;

                }
            }
            catch (Exception ex)
            {
                Repository.AutomateThisEvents.EventErrorLog(string.Format("GetTestCase() failed with following Exception: {0}", ex.Message), Repository.Util.Config.ErrorLogLocation);
            }
            return null;

        }
        public Feature GetFeaturesTestSuites(string featureId)
        {
            try
            {
                using (var db = new AutomateThisContext(ConnectionString))
                {
                    var feature = db.Features
                         .Include(f => f.TestSuites)
                             .ThenInclude(ts => ts.Testcases)
                                 .ThenInclude(tc => tc.Steps)
                                    .SingleOrDefault(f1 => f1.Id == featureId);
                    return feature;
                }
            }
            catch (Exception ex)
            {
                Repository.AutomateThisEvents.EventErrorLog(string.Format("GetFeatureTestSuites() failed with following Exception: {0}", ex.Message), Repository.Util.Config.ErrorLogLocation);
            }
            return null;

        }

    }


}
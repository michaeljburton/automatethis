
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Web;
using System.Diagnostics;
using AutomateThis.Model;
using AutomateThis.Protractor;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace AutomateThis.CustomObjects
{
    public abstract class CustomObject
    {
        public IWebDriver WebDriver {get;set;}
         public int Index {get;set;}
        public NgWebDriver ngDriver {get;set;}
        public Step CurrentStep {get;set;}
        public Repository Repository {get;set;}
        public Dictionary<string, Action> Functions;
        public WebDriverWrapper webDriverCmds;
        public StepResult StepResult;
        public CustomObject(IWebDriver webDriver, NgWebDriver ngDriver, Step step, Repository repository,StepResult stepResult)
        {
            this.WebDriver = webDriver;
            this.ngDriver = ngDriver;
            this.CurrentStep = step;
            this.Repository = repository;
            this.StepResult = stepResult;
            Functions = new Dictionary<string, Action>();
            webDriverCmds = new WebDriverWrapper(webDriver,ngDriver,repository);

        }
        public abstract void Run();
    
         public abstract void AddFunctions();
         
      
    }

}
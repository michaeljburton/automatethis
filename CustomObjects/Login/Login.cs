

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Web;
using System.Diagnostics;
using AutomateThis.Model;
using AutomateThis.Protractor;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using AutomateThis.CustomObjects;

namespace AutomateThis.CustomObjects.Login
{
    public class Login : CustomObject
    {
        const string LOGINSHOPV5 = "login_shopv5";
        const string LOGINSHOPV4 = "login_shopv4";
        public Login(IWebDriver webDriver, NgWebDriver ngDriver, Step step, Repository repository,StepResult stepResult) : base(webDriver, ngDriver, step, repository,stepResult)
        {
            AddFunctions();
        }
        public override void AddFunctions()
        {
            Functions.Add(LOGINSHOPV5, LoginShopV5);
            Functions.Add(LOGINSHOPV4, LoginShopV4);

        }
        public override void Run()
        {
            try
            {
                Functions[CurrentStep.CustomFunction].Invoke();
            }
            catch (Exception ex)
            {
             
                Repository.AutomateThisEvents.EventErrorLog(Repository.Util.MessageFormatterException("Login.Run()", ex), Repository.Util.Config.ErrorLogLocation);
                StepResult.Exception = ex;
                StepResult.Message = "Step Failed";
            }
        }


        void LoginShopV5()
        {
            //go to shop url based on Environment 
            webDriverCmds.Get(Repository.CurrentShopV5Url);
            //get username and password from current step
            string[] parameters = Repository.Util.ReplaceTokensInCSV(CurrentStep.CustomParameters, Repository.PageConfigs);
            var username = parameters[0];
            var password = parameters[1];
            //find username element
            NgWebElement usernameElement = webDriverCmds.FindElementByModel(Repository.Util.GetTokenValue(Repository.PageConfigs, "${LoginShopV5.usernameInput}"));
            //input user name 
            webDriverCmds.InputValue(usernameElement, null, username);
            //find password element 
            NgWebElement passwordElement = webDriverCmds.FindElementByModel(Repository.Util.GetTokenValue(Repository.PageConfigs, "${LoginShopV5.passwordInput}"));
            //input password
            webDriverCmds.InputValue(passwordElement, null, password);
            //find submit button
            NgWebElement submitBtnElement = webDriverCmds.FindElementByBinding(Repository.Util.GetTokenValue(Repository.PageConfigs, "${LoginShopV5.submitButton}"));
            //click button
            webDriverCmds.ClickElement(null,submitBtnElement);

        }
        void LoginShopV4()
        {

        }
    }

}
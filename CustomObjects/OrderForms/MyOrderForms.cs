using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Web;
using System.Diagnostics;
using AutomateThis.Model;
using AutomateThis.Protractor;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using AutomateThis.CustomObjects;
using System.Collections.ObjectModel;

namespace AutomateThis.CustomObjects.OrderForms
{
    public class MyOrderForms : CustomObject
    {
        const string LOAD_TO_BASKET_V5 = "load_to_basket_v5";
        const string LOAD_TO_BASKET_V4 = "load_to_basket_v4";


        public MyOrderForms(IWebDriver webDriver, NgWebDriver ngDriver, Step step, Repository repository, StepResult stepResult) : base(webDriver, ngDriver, step, repository, stepResult)
        {
            AddFunctions();
        }
        public override void AddFunctions()
        {
            Functions.Add(LOAD_TO_BASKET_V5, LoadToBasketV5);
            Functions.Add(LOAD_TO_BASKET_V4, LoadToBasketV4);

        }
        public override void Run()
        {
            try
            {
                Functions[CurrentStep.CustomFunction].Invoke();
            }
            catch (Exception ex)
            {

                Repository.AutomateThisEvents.EventErrorLog(Repository.Util.MessageFormatterException("MyOrderForms.Run()", ex), Repository.Util.Config.ErrorLogLocation);
                StepResult.Exception = ex;
                StepResult.Message = "Step Failed";
            }
        }
        public void LoadToBasketV5()
        {
            string[] parameters = Repository.Util.ReplaceTokensInCSV(CurrentStep.CustomParameters, Repository.PageConfigs);
            var search = parameters[0];
            int button = 1;// selects view button
            //nav to my order forms
            //click orders
            webDriverCmds.ClickElement(webDriverCmds.FindElement(Repository.Util.GetTokenValue(Repository.PageConfigs, "${ordersMenuV5.ordersBy}"),Repository.Util.GetTokenValue(Repository.PageConfigs, "${ordersMenuV5.orders}")), null);
            //click my order forms
            webDriverCmds.ClickElement(webDriverCmds.FindElement(Repository.Util.GetTokenValue(Repository.PageConfigs, "${ordersMenuV5.myOrderFormsBy}"),Repository.Util.GetTokenValue(Repository.PageConfigs, "${ordersMenuV5.myOrderForms}")), null);
            //find all table elements        
            //select an order form by string  and click button
            ClickOrderFormButtonByName(search,button);
            //Loop through order items based on an index(order form count) and add random qty between 1-9 to products the are avaiable to order 
            AddQtyToAllProductItems(1,9);
            //Click load to lasket button
            webDriverCmds.ClickElement(webDriverCmds.FindElement(Repository.Util.GetTokenValue(Repository.PageConfigs, "${orderFormsViewV5.loadToBasketButtonBy}"),Repository.Util.GetTokenValue(Repository.PageConfigs, "${orderFormsViewV5.loadToBasketButton}")), null);

        }
        void ClickOrderFormButtonByName(string myOrderFormName,int buttonIndex)
        {
            NgWebElement element = GetOrderFormByName(myOrderFormName);
            if(element == null)
                throw new Exception("Test failed as the order form "+myOrderFormName+" was not found!");
            webDriverCmds.ClickElement(null, element.FindElement(By.XPath("//div[contains(@class,'order-list')]/div[" + Index + "]/div[contains(@class,'order-row')]/div[contains(@class,'order-btn-wrapper')]/span[" + buttonIndex + "][button]")));
        }
        NgWebElement GetOrderFormByName(string myOrderFormName)
        {
            Index = 1;
            ReadOnlyCollection<NgWebElement> table = webDriverCmds.FindElementsByRepeater(Repository.Util.GetTokenValue(Repository.PageConfigs, "${orderFormsV5.orderFormsTable}"));
            foreach (var element in table)
            {
                var str = element.FindElement(NgBy.Binding("myOrder.Name")).Text;
                if (str == myOrderFormName)
                {
                    return element;
                }
                Index++;
            }    
            return null;  
            
        }
        void AddQtyToAllProductItems(int start,int end)
        {           
       
            ReadOnlyCollection<NgWebElement> table = webDriverCmds.FindElementsByRepeater(Repository.Util.GetTokenValue(Repository.PageConfigs, "${orderFormsViewV5.orderFormProductList}"));
            foreach (var element in table)
            {
                 int number = Repository.Util.GenerateRandomNumber(start,end);
                 //set all to blank
                 webDriverCmds.InputValue(null,element.FindElement(NgBy.Model("product.OrderQuantity")),"");
                  webDriverCmds.InputValue(null,element.FindElement(NgBy.Model("product.OrderQuantity")),number.ToString());
            }    
        }

        void LoadToBasketV4()
        {
            //nav to my order forms

            //select an order form by string

            //Loop through order items based on an index(order form count) and add random qty between 1-9 to products the are avaiable to order 

            //Click load to lasket button 


        }



    }
}
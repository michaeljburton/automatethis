
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Web;
using System.Diagnostics;
using System.Collections.ObjectModel;

using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using AutomateThis.Model;
using AutomateThis.Protractor;



namespace AutomateThis
{
    public class WebDriverWrapper
    {

        public IWebDriver WebDriver;
        public NgWebDriver NgWebDriver;
        public Repository Repository;
        Dictionary<string, Func<string, IWebElement>> ElementFunctions;
        Dictionary<string, Func<string, ReadOnlyCollection<IWebElement>>> ElementsFunctions;

        public WebDriverWrapper(Repository repository)
        {
            this.Repository = repository;
            this.WebDriver = Repository.WebDriver;
            this.NgWebDriver = Repository.NgWebDriver;
            this.ElementFunctions = new Dictionary<string, Func<string, IWebElement>>();
            this.ElementsFunctions = new Dictionary<string, Func<string, ReadOnlyCollection<IWebElement>>>();
            ElementFunctions.Add(WebDriverCommands.XPATH, FindElementByXPath);
            ElementFunctions.Add(WebDriverCommands.NAME, FindElementByName);
            ElementFunctions.Add(WebDriverCommands.ID, FindElementById);
            ElementFunctions.Add(WebDriverCommands.CSS, FindElementByCss);
            ElementFunctions.Add(WebDriverCommands.CLASS, FindElementByClass);
            ElementFunctions.Add(WebDriverCommands.LINK_TEXT, FindElementByLinkText);
            ElementFunctions.Add(WebDriverCommands.MODEL, FindElementByModel);
            ElementFunctions.Add(WebDriverCommands.BINDING, FindElementByBinding);
            ElementFunctions.Add(WebDriverCommands.REPEATER, FindElementByRepeater);

            ElementsFunctions.Add(WebDriverCommands.XPATH, FindElementsByXPath);
            ElementsFunctions.Add(WebDriverCommands.NAME, FindElementsByName);
            ElementsFunctions.Add(WebDriverCommands.ID, FindElementsById);
            ElementsFunctions.Add(WebDriverCommands.CSS, FindElementsByCss);
            ElementsFunctions.Add(WebDriverCommands.CLASS, FindElementsByClass);
            ElementsFunctions.Add(WebDriverCommands.LINK_TEXT, FindElementsByLinkText);
            //ElementsFunctions.Add(WebDriverCommands.MODEL, FindElementsByModel);
            //ElementsFunctions.Add(WebDriverCommands.BINDING, FindElementsByBinding);
            //ElementsFunctions.Add(WebDriverCommands.REPEATER, FindElementByRepeater);


        }
        private void WaitForPageLoad()
        {
            Stopwatch pageLoadTime = new Stopwatch();
            pageLoadTime.Start();
            bool pageIsLoaded = false;
            while (!pageIsLoaded)
            {
                bool isAjaxFinished = (bool)((IJavaScriptExecutor)WebDriver).
                ExecuteScript("return jQuery.active == 0");
                bool isPageReady = ((IJavaScriptExecutor)WebDriver).ExecuteScript("return document.readyState").Equals("complete");
                if (isAjaxFinished && isPageReady)
                    pageIsLoaded = true;
                long time = pageLoadTime.ElapsedMilliseconds;
                if (time > Repository.Util.Config.MaxPageLoadTime)
                    pageIsLoaded = true;
            }

            pageLoadTime.Stop();

        }

        public IWebElement FindElement(string by, string element)
        {

            IWebElement WebElement = ElementFunctions[by].Invoke(element);
            return WebElement;
        }
        public ReadOnlyCollection<IWebElement> FindElements(string by, string element)
        {

            ReadOnlyCollection<IWebElement> WebElements = ElementsFunctions[by].Invoke(element);
            return WebElements;
        }


        //Find Elements
        public IWebElement FindElementByXPath(string element)
        {
            WaitForPageLoad();
            WebDriverWait Wait = new WebDriverWait(WebDriver, TimeSpan.FromSeconds(Repository.Util.Config.SeleniumWaitTime));
            IWebElement WebElement = Wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(element)));
            return WebElement;
        }
        public ReadOnlyCollection<IWebElement> FindElementsByXPath(string element)
        {
            WaitForPageLoad();
            ReadOnlyCollection<IWebElement> WebElements = WebDriver.FindElements(By.XPath(element));
            return WebElements;
        }
        public IWebElement FindElementByCss(string element)
        {
            WaitForPageLoad();
            WebDriverWait Wait = new WebDriverWait(WebDriver, TimeSpan.FromSeconds(Repository.Util.Config.SeleniumWaitTime));
            IWebElement WebElement = Wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(element)));
            //NgWebDriver.WaitForAngular();
            // NgWebElement WebElement = NgWebDriver.FindElement(By.CssSelector(element));
            return WebElement;
        }
        public ReadOnlyCollection<IWebElement> FindElementsByCss(string element)
        {
            WaitForPageLoad();
            ReadOnlyCollection<IWebElement> WebElements = WebDriver.FindElements(By.CssSelector(element));
            return WebElements;
        }
        public IWebElement FindElementByClass(string element)
        {
            WaitForPageLoad();
            WebDriverWait Wait = new WebDriverWait(WebDriver, TimeSpan.FromSeconds(Repository.Util.Config.SeleniumWaitTime));
            IWebElement WebElement = Wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName(element)));
            return WebElement;
        }
        public ReadOnlyCollection<IWebElement> FindElementsByClass(string element)
        {
            WaitForPageLoad();
            ReadOnlyCollection<IWebElement> WebElements = WebDriver.FindElements(By.ClassName(element));
            return WebElements;
        }
        public IWebElement FindElementByName(string element)
        {
            WaitForPageLoad();
            WebDriverWait Wait = new WebDriverWait(WebDriver, TimeSpan.FromSeconds(Repository.Util.Config.SeleniumWaitTime));
            IWebElement WebElement = Wait.Until(ExpectedConditions.ElementIsVisible(By.Name(element)));
            return WebElement;
        }
        public ReadOnlyCollection<IWebElement> FindElementsByName(string element)
        {
            WaitForPageLoad();
            ReadOnlyCollection<IWebElement> WebElements = WebDriver.FindElements(By.Name(element));
            return WebElements;
        }
        public IWebElement FindElementById(string element)
        {
            WaitForPageLoad();
            WebDriverWait Wait = new WebDriverWait(WebDriver, TimeSpan.FromSeconds(Repository.Util.Config.SeleniumWaitTime));
            IWebElement WebElement = Wait.Until(ExpectedConditions.ElementIsVisible(By.Id(element)));
            return WebElement;
        }
        public ReadOnlyCollection<IWebElement> FindElementsById(string element)
        {
            WaitForPageLoad();
            ReadOnlyCollection<IWebElement> WebElements = WebDriver.FindElements(By.Id(element));
            return WebElements;
        }
        public IWebElement FindElementByLinkText(string element)
        {
            WaitForPageLoad();
            WebDriverWait Wait = new WebDriverWait(WebDriver, TimeSpan.FromSeconds(Repository.Util.Config.SeleniumWaitTime));
            IWebElement WebElement = Wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText(element)));
            return WebElement;
        }
        public ReadOnlyCollection<IWebElement> FindElementsByLinkText(string element)
        {
            WaitForPageLoad();
            ReadOnlyCollection<IWebElement> WebElements = WebDriver.FindElements(By.LinkText(element));
            return WebElements;
        }
        public NgWebElement FindElementByModel(string element)
        {

            NgWebElement WebElement = NgWebDriver.FindElement(NgBy.Model(element));
            return WebElement;
        }
        public ReadOnlyCollection<NgWebElement> FindElementsByModel(string element)
        {

            ReadOnlyCollection<NgWebElement> WebElements = NgWebDriver.FindElements(NgBy.Model(element));
            return WebElements;
        }
        public NgWebElement FindElementByBinding(string element)
        {
            NgWebElement WebElement = NgWebDriver.FindElement(NgBy.Binding(element));

            return WebElement;
        }
        public ReadOnlyCollection<NgWebElement> FindElementsByBinding(string element)
        {
            ReadOnlyCollection<NgWebElement> WebElements = NgWebDriver.FindElements(NgBy.Binding(element));
            return WebElements;
        }
        public ReadOnlyCollection<NgWebElement> FindElementsByRepeater(string element)
        {
            ReadOnlyCollection<NgWebElement> WebElements = NgWebDriver.FindElements(NgBy.Repeater(element));
            return WebElements;
        }

        public NgWebElement FindElementByRepeater(string element)
        {
            NgWebElement WebElement = NgWebDriver.FindElement(NgBy.Repeater(element));

            return WebElement;
        }
        public bool ElementExists(string by, string element)
        {
            // Boolean isPresent = NgWebDriver.FindElements().size() > 0;
            Boolean isPresent = FindElements(by, element).Count > 0;
            return isPresent;
        }
        //Behaviours e.g. Clicking etc... 
        public void Get(string url)
        {
            WebDriver.Navigate().GoToUrl(url);
        }
        /// <summary>
        /// Wrapper function for selenium function WebDriver.Navigate().Back();
        /// </summary>
        public void Back()
        {
            WebDriver.Navigate().Back();
        }
        /// <summary>
        /// Wrapper function for selenium function WebDriver.Navigate().Forward();
        /// </summary>
        public void Forward() { WebDriver.Navigate().Forward(); }
        /// <summary>
        /// Wrapper function for selenium function  WebDriver.Navigate().Refresh();
        /// </summary>
        public void Refresh() { WebDriver.Navigate().Refresh(); }
        /// <summary>
        /// Wrapper function for selenium function  WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());
        /// </summary>
        public void SelectNewWindow() { WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last()); }
        /// <summary>
        /// Finds element by subcommands e.g. xpath, css and id etc...
        /// </summary>
        /// <summary>
        /// Wrapper of selenium function SendKeys(element);
        /// </summary>
        public void InputValue(IWebElement webElement, NgWebElement ngWebElement, string input)
        {
            //input value
            if (webElement != null)
            {
                webElement.Clear();
                webElement.SendKeys(input);
                return;
            }
            if (ngWebElement != null)
            {
                //CurrentNgElement.Clear();
                ngWebElement.SendKeys(input);
                return;
            }


        }
        /// <summary>
        ///  Wrapper of selenium function SelectByText(text);
        /// Selects a Drop Down Value by currentElement
        /// </summary>
        public void SelectDropDownValue(IWebElement webElement, NgWebElement ngWebElement, string value)
        {

            //input value
            if (webElement != null)
            {
                var selectElement = new SelectElement(webElement);
                selectElement.SelectByText(value);
            }
            else
            {
                var selectElement = new SelectElement(ngWebElement);
                selectElement.SelectByText(value);
            }

        }
        /// <summary>
        /// Wrapper of selenium function Clear();
        /// clears the currentElement 
        /// </summary>
        public void ClearElement(IWebElement webElement)
        {
            // Clear current element
            webElement.Clear();
        }
        /// <summary>
        /// Wrapper of selenium function Click();
        /// clicks the currentElement 
        /// </summary>
        public void ClickElement(IWebElement webElement, NgWebElement ngWebElement)
        {
            // Click Element      
            if (webElement != null)
            {
                Actions actions = new Actions(WebDriver);
                actions.MoveToElement(webElement).Perform();
                webElement.Click();
                return;
            }
            if (ngWebElement != null)
            {
                //CurrentNgElement.Clear();
                ngWebElement.Click();
                return;
            }



        }
        /// <summary>
        /// Wrapper of selenium function GetAttribute("value");
        /// gets attribute value 
        /// </summary>
        public void GetAttribute(IWebElement webElement, NgWebElement ngWebElement, string compareValue)
        {
            string actualValue;
            if (webElement != null)
            {
                actualValue = webElement.Text;
            }
            else
            {
                actualValue = ngWebElement.Text;
            }
            //assign attribute          
            if (actualValue != compareValue)
            {
                throw new Exception(string.Format("Expected Result: {0} , Actual value found: {1}", compareValue, actualValue));
            }
        }
        public string GetValue(IWebElement webElement, NgWebElement ngWebElement)
        {
            string actualValue;
            if (webElement != null)
            {
                return actualValue = webElement.Text;
            }
            else
            {
                return actualValue = ngWebElement.Text;
            }

        }
        /// <summary>
        /// Wrapper function for selenium function WebDriver.SwitchTo().Frame(CurrentElement);
        /// finds element using SubCommands then calls selenium function and uses the CurrentElement
        /// </summary>
        public void SwitchToiFrame(IWebElement webElement)
        {
            WebDriver.SwitchTo().Frame(webElement);
        }
        /// <summary>
        /// Wrapper function for selenium function WebDriver.SwitchTo().Alert();
        /// </summary>
        public void SwitchToAlert() { WebDriver.SwitchTo().Alert(); }
        /// <summary>
        /// Wrapper function for selenium function WebDriver.SwitchTo().Alert().Accept();
        /// </summary>
        public void AlertClickAccept() { WebDriver.SwitchTo().Alert().Accept(); }
        /// <summary>
        /// Wrapper function for selenium function WebDriver.SwitchTo().Alert().Accept();
        /// </summary>
        public void AlertClickDismiss() { WebDriver.SwitchTo().Alert().Dismiss(); }
        /// <summary>
        /// Wrapper function for selenium function WebDriver.SwitchTo().Alert().Text and compares with an expected value
        /// </summary>
        public void AlertText(string input)
        {
            string alertText = WebDriver.SwitchTo().Alert().Text;
            if (alertText != input)
                throw new Exception(string.Format("Alert text doesn't match Expected result:{0} , Actual result: {1}", input, alertText));
        }
        public void ElementVisible(IWebElement element, string compareValue)
        {

            bool isVisible = element.Displayed;
            bool compare = Convert.ToBoolean(compareValue);
            if (isVisible != compare)
                throw new Exception(string.Format("Visible should be {0} but it was {1}", compare, isVisible));


        }
        public void CheckURL(string element, string compareValue, string input)
        {
            bool compare = Convert.ToBoolean(compareValue);
            string currentUrl = WebDriver.Url;
            string url = Repository.GetApplicationUrl(element) + input; 
            bool isMatch = currentUrl.IndexOf(url, StringComparison.OrdinalIgnoreCase) >= 0;
            if (compare != isMatch)
                throw new Exception(string.Format("Expected url was {0} and actual url is {1} but match is set to {2}", url, currentUrl, compare));
        }
        public void ScrollTo(int x = 0, int y = 0)
        {
            var js = String.Format("window.scrollTo({0}, {1})", x, y);
            ((IJavaScriptExecutor)WebDriver).ExecuteScript(js);
        }
        public void ScrollToElement(IWebElement element)
        {
            if (element.Location.Y > 200)
            {
                ScrollTo(0, element.Location.Y - 100);
            }

        }





    }

}
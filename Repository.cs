using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using AutomateThis.Model;
using AutomateThis.Protractor;
namespace AutomateThis
{
    public class Repository
    {
        public Commands Cmds {get;set;}
        public string CurrentProjectName {get; set;}
        public string CurrentFeatureName {get; set;}   
        public string CurrentTestsuite { get; set; }
        public string CurrentTestCase { get; set; }
        public string CurrentLogFileLocation {get;set;}
        public string TestRunLocation {get;set;}
        public enum MessageType { Request = 1, Result = 2,Response = 3 };
        public enum MessageFormat { JSON = 1, XML = 2 };
        public Util Util;
        public EnvironmentVars EnvironmentVars {get;set;}
        public Dictionary<string,string> AppicationUrls {get;set;}
        public IAutomateThisEvents AutomateThisEvents;
        public List<PageConfig> PageConfigs;
        public WebDriverWrapper WebDriverWrapper;
        public IWebDriver WebDriver{get;set;}
        public NgWebDriver NgWebDriver{get;set;}
        public Repository(IAutomateThisEvents automateThisEvents,Commands cmds,Config config)
        {
            this.AutomateThisEvents = automateThisEvents;
            this.Cmds = cmds;
            Util = new Util(automateThisEvents,config);
            PageConfigs = Util.LoadPageConfigs();             
            EnvironmentVars = Util.LoadEnvironmentVars();
            AppicationUrls = GetApplicationUrls(EnvironmentVars,cmds.Environment,cmds.SubEnvironment);
            
        }
        public Dictionary<string,string> GetApplicationUrls(EnvironmentVars environmentVars,string env,string subEnv)
        {
            //public Dictionary<string, Dictionary<string, Dictionary<string,string>>> Environments = new Dictionary<string,  Dictionary<string, Dictionary<string,string>>>();       
            var environment =  environmentVars.Environments.FirstOrDefault(d => d.Key.Contains(env)).Value;
            var subEnvironment = environment.FirstOrDefault(d => d.Key.Contains(subEnv)).Value;
            Dictionary<string,string> urls = new Dictionary<string, string>();
            foreach(var e in subEnvironment)
            {
                urls.Add(e.Key,e.Value);
            }          
            return urls;
        }
        public string GetApplicationUrl(string key)
        {
            string url = AppicationUrls.FirstOrDefault(d => d.Key.Contains(key)).Value;
            return url;
        }
  
    }

}
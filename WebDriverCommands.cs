namespace AutomateThis
{
    public static class WebDriverCommands
    {

        
        
        //Commands
          /// <summary>
        /// See if on correct url
        /// </summary>
        public static string CHECK_URL = "check_url";
        /// <summary>
        /// See if a element is true or false 
        /// </summary>
        public static string ELEMENT_VISIBLE = "element_visible";
        /// <summary>
        /// Get value from a element and check if true or false 
        /// </summary>
        public static string VALUE_CONTAINS = "value_contains";
         /// <summary>
        /// Check if a element exists, returns bool 
        /// </summary>
        public static string ELEMENT_EXISTS = "element_exists";
        /// <summary>
        /// Switchs to iframe by current element
        /// </summary>
        public static string SWITCH_TO_IFRAME = "switch_to_iframe";
        /// <summary>
        /// Switchs to alert by current element
        /// </summary>
        public static string SWITCH_TO_ALERT = "switch_to_alert";
         /// <summary>
        /// Click ok button on alert
        /// </summary>
        public static string ALERT_ACCEPT = "alert_accept";
            /// <summary>
        /// Click cancel button on alert
        /// </summary>
        public static string ALERT_DISMISS = "alert_dismiss";
        /// <summary>
        /// compare alert text with Step.Input
        /// </summary>
        public static string ALERT_TEXT= "alert_text";

        /// <summary>
        /// make get request to a url
        /// </summary>
        public static string GET = "get";
        /// <summary>
        /// Make post request to a url 
        /// </summary>
        public static string POST = "post";
        /// <summary>
        /// Inputs values into a textfield etc.
        /// </summary>
        public static string INPUT = "input";
        /// <summary>
        /// Clears the current element
        /// </summary>
        public static string CLEAR_ELEMENT = "clear_element";
        /// <summary>
        /// Clicks the current element
        /// </summary>
        public static string CLICK_ELEMENT = "click_element";
        /// <summary>
        /// Selects the current window, uses current element
        /// </summary>
        public static string SELECT_NEW_WINDOW = "select_new_window";
        /// <summary>
        /// Finds a element on the dom
        /// </summary>
        public static string FIND_ELEMENT = "find_element";
        /// <summary>
        /// Back command on a browser
        /// </summary>
        public static string BACK = "back";
        /// <summary>
        /// Forward cmd on a browser 
        /// </summary>
        public static string FORWARD = "forward";
        /// <summary>
        /// Refresh cmd on a browser 
        /// </summary>
        public static string REFRESH = "refresh";
        /// <summary>
        /// Screenshot browser 
        /// </summary>
        public static string SCREENSHOT = "screenshot";
        /// <summary>
        ///  get_attribute value and compare to step input 
        /// </summary>
        public static string GET_ATTRIBUTE = "get_attribute";
        /// <summary>
        /// Select a value in a drop down by element
        /// </summary>
        public static string SELECT = "select";
        /// <summary>
        /// Go to shopv5 url using get
        /// </summary>
        public static string SHOPV5_LOGIN = "shopv5";
        /// <summary>
        /// go to adminv4 url using get
        /// </summary>
        public static string ADMINV5_LOGIN = "adminv5";
        /// <summary>
        /// go to shopv4 url using get
        /// </summary>
        public static string SHOPV4_LOGIN = "shopv4";
        /// <summary>
        /// go to adminv4 url using get
        /// </summary>
        public static string ADMINV4_LOGIN = "adminv4";
        public static string GET_TEST_DATA = "get_test_data";
         public static string TEST_API = "testApi";
        public static string CUSTOM = "custom";
        //SubCommands
        public static string XPATH = "xpath";
        public static string NAME = "name";
        public static string ID = "id";
        public static string CSS = "css";
        public static string CLASS = "class";
        public static string LINK_TEXT = "link_text";
        public static string MODEL = "model";
        public static string BINDING = "binding";
        public static string REPEATER = "repeater";
    }
}
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Web;
using System.Diagnostics;
using OpenQA.Selenium.Support.UI;
using AutomateThis.Model;
using AutomateThis.Protractor;
using OpenQA.Selenium.Interactions;
using AutomateThis.CustomSteps;
using System.Collections.ObjectModel;
namespace AutomateThis
{
    public class StepRunner
    {

        /// <summary>
        /// Step Result Object used for reporting
        /// </summary>
        public StepResult StepResult;
        /// <summary>
        /// Repo object so Steprunner has access to util object functions and repo values 
        /// </summary>
        public Repository Repository;
        /// <summary>
        /// Current Step passed through the Run() method
        /// </summary>
        public Step CurrentStep;
        /// <summary>
        ///  Dictionary<string, Action> Commands;
        /// Dictionary of Cmds used to call selenium functions e.g. wrapper functions
        /// </summary>
        Dictionary<string, Action> Cmds;
        /// <summary>
        /// Dictionary<string, Action> SubCommands;
        /// Dictionary of Cmds used to call selenium functions e.g. selenium find element functions 
        /// </summary>
        Dictionary<string, Action> SubCommands;
        /// <summary>
        /// CurrentUrl is used to hold the full url of the current step 
        /// </summary>
        /// <returns></returns>      
        Dictionary<string, Action> CustomObjects;
        public string CurrentUrl { get; set; }
        /// <summary>
        /// Used to hold the CurrentAttribute that is being found by the current step e.g.label text etc.
        /// </summary>
        /// <returns></returns>
        public string CurrentAttribute { get; set; }
        /// <summary>
        /// CurrentElement is the html element currently selected in the current step
        /// </summary>
        public IWebElement CurrentElement;
        /// <summary>
        /// Constructor for StepRunner
        /// * Creates instances of Commands and SubCommands
        /// * Adds all of the commands and subcoomands by calling AddCommands(); and AddSubCommands();
        /// </summary>
        public NgWebElement CurrentNgElement;
        public WebDriverWait Wait;
        public WebDriverWrapper WebDriverWrapper;
        public ReadOnlyCollection<NgWebElement> CurrentNgElements;
        public StepRunner()
        {

            this.Cmds = new Dictionary<string, Action>();
            this.SubCommands = new Dictionary<string, Action>();
            this.CustomObjects = new Dictionary<string, Action>();
            AddCommands();
            AddSubCommands();
        }
        /// <summary>
        /// This method excutes the selenium functions based on the step object 
        /// </summary>
        /// <param name="webDriver">Selenium webdriver object </param>
        /// <param name="step">Current step to be excuted</param>
        /// <param name="repository">Current repo used in current test run</param>
        /// <returns>StepResult object which contains the step result e.g. pass/fail and fail exception if thrown</returns>
        public StepResult Run(Step step, Repository repository)
        {

            this.Repository = repository;
            step.Testcase = null;
            this.CurrentStep = step;
            FormatStep();
            this.Wait = new WebDriverWait(Repository.WebDriver, TimeSpan.FromSeconds(Repository.Util.Config.SeleniumWaitTime));
            WebDriverWrapper = new WebDriverWrapper(repository);
            ReplaceTokens(step);
            Repository.AutomateThisEvents.EventLog(Repository.Util.MessageFormatter(step, Repository.MessageType.Request), Repository.CurrentLogFileLocation);
            StepResult = new StepResult
            {
                StepId = step.Id,
                Conditional = step.Conditional,
                By = step.By,
                Command = step.Command

            };

            Stopwatch stepStopWatch = new Stopwatch();
            stepStopWatch.Start();
            try
            {

                Cmds[CurrentStep.Command].Invoke();
                stepStopWatch.Stop();
            }
            catch (Exception ex)
            {
                stepStopWatch.Stop();
                Repository.AutomateThisEvents.EventErrorLog(Repository.Util.MessageFormatterException("StepRunner.Run()", ex), Repository.Util.Config.ErrorLogLocation);
                StepResult.Exception = ex;
                StepResult.Message = "Step Failed";
                return StepResult;
            }


            TimeSpan stepTimeSpan = stepStopWatch.Elapsed;
            StepResult.BehaviourReturnedTime = Repository.Util.FormatTimeSpan(stepTimeSpan);
            if (step.Delay > 0)
            {
                Thread.Sleep(step.Delay);
            }
            if (StepResult.Exception != null)
            {
                StepResult.Result = false;
                StepResult.Message = "Step Failed";
            }
            else
            {
                StepResult.Message = "Step Passed";
                StepResult.Result = true;
            }

            return StepResult;

        }
        void FormatStep()
        {




        }
        /// <summary>
        /// Adds commands to the Dictionary<string, Action> Commands; 
        /// e.g.
        ///Commands.Add("cmdName", functionName); adds a command, functionName is the function name as it uses the Action type
        ///You can Invoke like this Commands["cmdName"].Invoke();
        ///Come here if you want to add new commands
        /// </summary>
        void AddCommands()
        {
            // Add New Command Methods Here 
            Cmds.Add(WebDriverCommands.SWITCH_TO_IFRAME, SwitchToiFrame);
            Cmds.Add(WebDriverCommands.SWITCH_TO_ALERT, SwitchToAlert);
            Cmds.Add(WebDriverCommands.ALERT_ACCEPT, AlertClickAccept);
            Cmds.Add(WebDriverCommands.ALERT_DISMISS, AlertClickDismiss);
            Cmds.Add(WebDriverCommands.ALERT_TEXT, AlertText);
            Cmds.Add(WebDriverCommands.GET, Get);
            Cmds.Add(WebDriverCommands.POST, Post);
            Cmds.Add(WebDriverCommands.INPUT, InputValue);
            Cmds.Add(WebDriverCommands.CLEAR_ELEMENT, ClearElement);
            Cmds.Add(WebDriverCommands.CLICK_ELEMENT, ClickElement);
            Cmds.Add(WebDriverCommands.SELECT_NEW_WINDOW, SelectNewWindow);
            Cmds.Add(WebDriverCommands.FIND_ELEMENT, FindElement);
            Cmds.Add(WebDriverCommands.BACK, Back);
            Cmds.Add(WebDriverCommands.FORWARD, Forward);
            Cmds.Add(WebDriverCommands.REFRESH, Refresh);
            Cmds.Add(WebDriverCommands.SCREENSHOT, StepScreenshot);
            Cmds.Add(WebDriverCommands.GET_ATTRIBUTE, GetAttribute);
            Cmds.Add(WebDriverCommands.SELECT, SelectDropDownValue);
            Cmds.Add(WebDriverCommands.SHOPV5_LOGIN, GetShopV5Login);
            Cmds.Add(WebDriverCommands.ADMINV5_LOGIN, GetAdminV5Login);
            Cmds.Add(WebDriverCommands.SHOPV4_LOGIN, GetShopV4Login);
            Cmds.Add(WebDriverCommands.ADMINV4_LOGIN, GetAdminV4Login);
            Cmds.Add(WebDriverCommands.GET_TEST_DATA, GetTestData);
            Cmds.Add(WebDriverCommands.ELEMENT_EXISTS, ElementExists);
            Cmds.Add(WebDriverCommands.VALUE_CONTAINS, ValueContains);
            Cmds.Add(WebDriverCommands.ELEMENT_VISIBLE, ElementVisible);
            Cmds.Add(WebDriverCommands.CHECK_URL, CheckURL);
            Cmds.Add(WebDriverCommands.CUSTOM, RunCustom);
        }
        /// <summary>
        /// Adds commands to the Dictionary<string, Action> SubCommands; 
        /// e.g.
        ///SubCommands.Add("cmdName", functionName); adds a command, functionName is the function name as it uses the type Action with no parameters e.g. void functions
        ///You can Invoke like this SubCommands["cmdName"].Invoke();
        ///Come here if you want to add new subcommands e.g. find a element by a different method 
        /// </summary>
        void AddSubCommands()
        {
            // Add New Sub Commands Here
            SubCommands.Add(WebDriverCommands.XPATH, FindElementByXPath);
            SubCommands.Add(WebDriverCommands.NAME, FindElementByName);
            SubCommands.Add(WebDriverCommands.ID, FindElementById);
            SubCommands.Add(WebDriverCommands.CSS, FindElementByCss);
            SubCommands.Add(WebDriverCommands.CLASS, FindElementByClass);
            SubCommands.Add(WebDriverCommands.LINK_TEXT, FindElementByLinkText);
            SubCommands.Add(WebDriverCommands.MODEL, FindElementByModel);
            SubCommands.Add(WebDriverCommands.BINDING, FindElementByBinding);
            SubCommands.Add(WebDriverCommands.REPEATER, FindElementByRepeater);
        }
        /// <summary>
        /// Wrapper function for selenium function WebDriver.SwitchTo().Frame(CurrentElement);
        /// finds element using SubCommands then calls selenium function and uses the CurrentElement
        /// </summary>
        public void SwitchToiFrame()
        {
            FindElement();
            WebDriverWrapper.SwitchToiFrame(CurrentElement);
        }
        /// <summary>
        /// Wrapper function for selenium function WebDriver.SwitchTo().Alert();
        /// </summary>
        public void SwitchToAlert() { WebDriverWrapper.SwitchToAlert(); }
        /// <summary>
        /// Wrapper function for selenium function WebDriver.SwitchTo().Alert().Accept();
        /// </summary>
        public void AlertClickAccept() { WebDriverWrapper.AlertClickAccept(); }
        /// <summary>
        /// Wrapper function for selenium function WebDriver.SwitchTo().Alert().Accept();
        /// </summary>
        public void AlertClickDismiss() {  WebDriverWrapper.AlertClickDismiss(); }
        /// <summary>
        /// Wrapper function for selenium function WebDriver.SwitchTo().Alert().Text and compares with an expected value
        /// </summary>
        public void AlertText()
        {
            WebDriverWrapper.AlertText(CurrentStep.Input);

        }
        public void ElementVisible()
        {
            //find element
            FindElement();
            WebDriverWrapper.ElementVisible(CurrentElement,CurrentStep.Data);


        }
        public void CheckURL()
        {
              WebDriverWrapper.CheckURL(CurrentStep.Element,CurrentStep.Data,CurrentStep.Input);
        }
        
        void ElementExists()
        {
            bool isPresent = WebDriverWrapper.ElementExists(CurrentStep.By, CurrentStep.Element);
            bool compare = Convert.ToBoolean(CurrentStep.Data);
            if (isPresent != compare)
                throw new Exception(string.Format("Element exists should be {0} but it was {1}", compare, isPresent));

        }
        void ValueContains()
        {
            //find element
            FindElement();
            bool isMatch = false;
            bool compare = Convert.ToBoolean(CurrentStep.Data);
            string value = WebDriverWrapper.GetValue(CurrentElement, CurrentNgElement);
            if (value.IndexOf(CurrentStep.Input, StringComparison.OrdinalIgnoreCase) >= 0)
                isMatch = true;
            if (isMatch != compare)
                throw new Exception(string.Format("Value should be {0} but it was {1}", compare, isMatch));


        }
        /// <summary>
        /// Wrapper function for selenium function WebDriver.Navigate().GoToUrl(CurrentStep.Element);
        /// </summary>
        public void Get()
        {
            WebDriverWrapper.Get(CurrentStep.Element);
        }
        public void GetShopV5Login()
        {
            WebDriverWrapper.Get(Repository.GetApplicationUrl(WebDriverCommands.SHOPV5_LOGIN));
        }
        public void GetShopV4Login()
        {
            WebDriverWrapper.Get(Repository.GetApplicationUrl(WebDriverCommands.SHOPV4_LOGIN));
        }
        public void GetAdminV5Login()
        {
            WebDriverWrapper.Get(Repository.GetApplicationUrl(WebDriverCommands.ADMINV5_LOGIN));
        }
        public void GetAdminV4Login()
        {
            WebDriverWrapper.Get(Repository.GetApplicationUrl(WebDriverCommands.ADMINV4_LOGIN));
        }

        /// <summary>
        /// Used to post data to a url 
        /// ToDo: this currently doesnt do anything
        /// </summary>
        public void Post()
        {
            string result = Repository.Util.Post(CurrentStep.Element, CurrentStep.Data, "application//json");
        }
        /// <summary>
        /// Wrapper function for selenium function WebDriver.Navigate().Back();
        /// </summary>
        public void Back() { WebDriverWrapper.Back(); }
        /// <summary>
        /// Wrapper function for selenium function WebDriver.Navigate().Forward();
        /// </summary>
        public void Forward() { WebDriverWrapper.Forward(); }
        /// <summary>
        /// Wrapper function for selenium function  WebDriver.Navigate().Refresh();
        /// </summary>
        public void Refresh() { WebDriverWrapper.Refresh(); }
        /// <summary>
        /// Wrapper function for selenium function  WebDriver.SwitchTo().Window(WebDriver.WindowHandles.Last());
        /// </summary>
        public void SelectNewWindow() { WebDriverWrapper.SelectNewWindow();  }
        /// <summary>
        /// Finds element by subcommands e.g. xpath, css and id etc...
        /// </summary>
        public void FindElement()
        {
            Stopwatch stepStopWatch = new Stopwatch();
            stepStopWatch.Start();
            SubCommands[CurrentStep.By].Invoke();
            stepStopWatch.Stop();
            TimeSpan stepTimeSpan = stepStopWatch.Elapsed;
            StepResult.ElementReturnedTime = Repository.Util.FormatTimeSpan(stepTimeSpan);
        }
        /// <summary>
        /// Takes a screenshot this can be step or used on all step types
        /// ToDo: Need to port this to .net Core 
        /// </summary>
        public void StepScreenshot()
        {

        }
        /// <summary>
        /// Replaces the query strings with values from the pageconfig's 
        /// </summary>
        /// <param name="s">current step</param>
        void ReplaceTokens(Step s)
        {
            if (s.Input != null && s.Input.Contains("${") && s.Input.Contains("}"))
            {
                s.Input = Repository.Util.GetTokenValue(Repository.PageConfigs, s.Input);
            }
            else if (s.Input != null && s.Input.Contains("#{") && s.Input.Contains("}"))
            {
                s.Input = Repository.Util.GetSavedValue(Repository.PageConfigs, s.Input);
            }
            if (s.By != null && s.By.Contains("${") && s.By.Contains("}"))
            {
                s.By = Repository.Util.GetTokenValue(Repository.PageConfigs, s.By);
            }
            else if (s.By != null && s.By.Contains("#{") && s.By.Contains("}"))
            {
                s.By = Repository.Util.GetSavedValue(Repository.PageConfigs, s.By);
            }
            if (s.Element != null && s.Element.Contains("${") && s.Element.Contains("}"))
            {
                s.Element = Repository.Util.GetTokenValue(Repository.PageConfigs, s.Element);
            }
            else if (s.Element != null && s.Element.Contains("#{") && s.Element.Contains("}"))
            {
                s.Element = Repository.Util.GetSavedValue(Repository.PageConfigs, s.Element);
            }
        }
       
        /// <summary>
        /// Wrapper of selenium function SendKeys(element);
        /// </summary>
        void InputValue()
        {
            //find element
            FindElement();
            //input value
            WebDriverWrapper.InputValue(CurrentElement, CurrentNgElement, CurrentStep.Input);

        }
        /// <summary>
        ///  Wrapper of selenium function SelectByText(text);
        /// Selects a Drop Down Value by currentElement
        /// </summary>
        void SelectDropDownValue()
        {
            //find element
            FindElement();
            //input value
            WebDriverWrapper.SelectDropDownValue(CurrentElement, CurrentNgElement, CurrentStep.Input);
        }
        /// <summary>
        /// Wrapper of selenium function Clear();
        /// clears the currentElement 
        /// </summary>
        void ClearElement()
        {
            //find element
            FindElement();
            // Clear current element
            WebDriverWrapper.ClearElement(CurrentElement);
        }
        /// <summary>
        /// Wrapper of selenium function Click();
        /// clicks the currentElement 
        /// </summary>
        public void ClickElement()
        {
            //find element
            FindElement();
            // Click Element
            WebDriverWrapper.ClickElement(CurrentElement, CurrentNgElement);
        }
        /// <summary>
        /// Wrapper of selenium function GetAttribute("value");
        /// gets attribute value 
        /// </summary>
        void GetAttribute()
        {

            //find element
            FindElement();
            //compare element
            WebDriverWrapper.GetAttribute(CurrentElement, CurrentNgElement, CurrentStep.Input);
        }
        void DelayStep()
        {
            if (CurrentStep.Delay > 0)
            {
                Thread.Sleep(CurrentStep.Delay);
            }

        }
        //Get Test Data
        void GetTestData()
        {
            string env = Repository.Cmds.SubEnvironment + Repository.Cmds.Environment;
            CurrentStep.CustomParameters = CurrentStep.CustomParameters.Replace("{Environment}", env);
            string url = Repository.GetApplicationUrl(WebDriverCommands.TEST_API) + CurrentStep.CustomCommand + CurrentStep.CustomFunction;
            string result = Repository.Util.Post(url, CurrentStep.CustomParameters, "text/json");
            if (result != "[]")
            {
                Repository.Util.SetPageConfigSavedValue(Repository.PageConfigs, CurrentStep.Input, result);
                Repository.AutomateThisEvents.EventLog(Repository.Util.MessageFormatter(result, Repository.MessageType.Result));
                return;
            }
            throw new Exception(string.Format("No Test Data from {0} with the following data request {1}", url, CurrentStep.CustomParameters));
        }

        //SubCommands
        void FindElementByXPath()
        {
            //CurrentElement = Wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(CurrentStep.Element)));
            //CurrentElement = WebDriver.FindElement(By.XPath(CurrentStep.Element)); 
            CurrentElement = WebDriverWrapper.FindElementByXPath(CurrentStep.Element);
        }
        void FindElementByCss()
        {
            //CurrentElement = Wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector(CurrentStep.Element)));
            //CurrentElement = WebDriver.FindElement(By.CssSelector(CurrentStep.Element));
            CurrentElement = WebDriverWrapper.FindElementByCss(CurrentStep.Element);
        }
        void FindElementByName()
        {
            //CurrentElement = Wait.Until(ExpectedConditions.ElementIsVisible(By.Name(CurrentStep.Element)));
            //CurrentElement = WebDriver.FindElement(By.Name(CurrentStep.Element)); 
            CurrentElement = WebDriverWrapper.FindElementByName(CurrentStep.Element);
        }
        void FindElementById()
        {
            //CurrentElement = Wait.Until(ExpectedConditions.ElementIsVisible(By.Id(CurrentStep.Element)));
            //CurrentElement = WebDriver.FindElement(By.Id(CurrentStep.Element)); 
            CurrentElement = WebDriverWrapper.FindElementById(CurrentStep.Element);
        }
        void FindElementByLinkText()
        {    //CurrentElement = Wait.Until(ExpectedConditions.ElementIsVisible(By.LinkText(CurrentStep.Element)));
            //CurrentElement = WebDriver.FindElement(By.LinkText(CurrentStep.Element)); 
            CurrentElement = WebDriverWrapper.FindElementByLinkText(CurrentStep.Element);
        }
        void FindElementByClass()
        {
            //CurrentElement = Wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName(CurrentStep.Element)));
            //CurrentElement = WebDriver.FindElement(By.ClassName(CurrentStep.Element)); 
            CurrentElement = WebDriverWrapper.FindElementByClass(CurrentStep.Element);
        }
        void FindElementByModel()
        {
            //CurrentNgElement = NgWebDriver.FindElement(NgBy.Model(CurrentStep.Element));
            CurrentNgElement = WebDriverWrapper.FindElementByModel(CurrentStep.Element);

        }
        void FindElementByBinding()
        {
            //CurrentNgElement = NgWebDriver.FindElement(NgBy.Binding(CurrentStep.Element));
            CurrentNgElement = WebDriverWrapper.FindElementByBinding(CurrentStep.Element);
        }
        void FindElementByRepeater()
        {
            //CurrentNgElement = NgWebDriver.FindElement(NgBy.Repeater(CurrentStep.Element));         
            CurrentNgElement = WebDriverWrapper.FindElementByRepeater(CurrentStep.Element);
        }

        void FindElementsByRepeater()
        {
            //CurrentNgElement = NgWebDriver.FindElement(NgBy.Repeater(CurrentStep.Element));         
            CurrentNgElements = WebDriverWrapper.FindElementsByRepeater(CurrentStep.Element);
        }

        // Custom Functions
        void RunCustom()
        {
            //AutomateThis.CustomObjects.Login
            Type type = Type.GetType("AutomateThis.CustomSteps." + CurrentStep.CustomCommand);
            //base(webDriver, ngDriver, step, repository)
            dynamic instance = Activator.CreateInstance(type,CurrentStep, Repository, StepResult);
            instance.Run();
        }

    }

}
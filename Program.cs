﻿using System;
using AutomateThis;
using AutomateThis.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace AutomateThis
{
    //dotnet build -r win10-x64
    class Program
    {
        static void Main(string[] args)
        {
            AutomateThis automateThis = new AutomateThis(); 
            // run single test case:
            //string[] args1 = {"-env='qa'","-cmd='rtc'","-webdriver='chrome'","-subenv='au'","-query='QA-721'","-repeat='1'"};

            //string[] args1 = {"-env='ie'","-cmd='rtc'","-webdriver='chrome'","-subenv='au'","-query='PT-QA-393'","-repeat='2'"};
            //string[] args1 = {"-env='prod'","-cmd='rtc'","-webdriver='chrome'","-subenv='au'","-query='AU-Prod-QA-54'","-repeat='10'"};
            //string[] args1 = {"-env='prod'","-cmd='rtc'","-webdriver='chrome'","-subenv='fsuk'","-query='FSUK-Prod-QA-54'","-repeat='10'"};

            // run all test cases:
            //string[] args1 = {"-env='qa'","-cmd='ra'","-webdriver='chrome'","-subenv='auto'"};
            
            // run all in single project:
            //string[] args1 = {"-env='ie'","-cmd='pra'","-webdriver='chrome'","-subenv='pt'","-query='Performance Tests'"};
            //string[] args1 = {"-env='prod'","-cmd='pra'","-webdriver='chrome'","-subenv='auprod'","-query='AUPROD'"};
            
            // run test suite:
            //string[] args1 = {"-env='qa'","-cmd='rts'","-webdriver='chrome'","-subenv='auprod'","-query='Test_AU_PROD_Env'"};
            //string[] args1 = {"-env='prod'","-cmd='rts'","-webdriver='chrome'","-subenv='auprod'","-query='QA_New2_V5_AUPROD'"};



            string[] args1 = {"-env='qa'","-cmd='rtc'","-webdriver='firefox'","-subenv='au'","-query='QA-141'"};
            automateThis.Run(args1);
           

        }
    }
}